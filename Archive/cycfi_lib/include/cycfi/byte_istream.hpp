/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#if !defined(BYTE_ISTREAM_HPP_OCTOBER_8_2012)
#define BYTE_ISTREAM_HPP_OCTOBER_8_2012

#include <stdint.h>
#include <cstring>
#include <cycfi/little_endian.hpp>
#include <cycfi/opaque.hpp>

namespace cycfi
{
   ////////////////////////////////////////////////////////////////////////////
   // Byte oriented output stream. Only fundamental types and opaque objects
   // are supported. Note that reading C strings is not supported; use opaque
   // instead. Fundamental types more than 8 bits (e.g. int32_t) are streamed
   // in as little endian.
   ////////////////////////////////////////////////////////////////////////////
   template <typename Port>
   class byte_istream
   {
   public:

      byte_istream(Port& port)
         : port(port){}

      byte_istream& operator>>(bool& val)
      {
         port.read((uint8_t&)val);
         return *this;
      }

      byte_istream& operator>>(char& val)
      {
         port.read((uint8_t&)val);
         return *this;
      }

      byte_istream& operator>>(uint8_t& val)
      {
         port.read(val);
         return *this;
      }

      // template <typename T>
      // byte_istream& operator>>(T& val)
      // {
         // port.read(reinterpret_cast<uint8_t*>(&val), sizeof(T));
         // little_endian(val);
         // return *this;
      // }
      
      byte_istream& operator>>(int16_t& val)
      {
         port.read(reinterpret_cast<uint8_t*>(&val), sizeof(int16_t));
         little_endian(val);
         return *this;
      }
      
      byte_istream& operator>>(uint16_t& val)
      {
         port.read(reinterpret_cast<uint8_t*>(&val), sizeof(uint16_t));
         little_endian(val);
         return *this;
      }
      
      byte_istream& operator>>(int32_t& val)
      {
         port.read(reinterpret_cast<uint8_t*>(&val), sizeof(int32_t));
         little_endian(val);
         return *this;
      }
      
      byte_istream& operator>>(uint32_t& val)
      {
         port.read(reinterpret_cast<uint8_t*>(&val), sizeof(uint32_t));
         little_endian(val);
         return *this;
      }
      
      byte_istream& operator>>(int64_t& val)
      {
         port.read(reinterpret_cast<uint8_t*>(&val), sizeof(int64_t));
         little_endian(val);
         return *this;
      }
      
      byte_istream& operator>>(uint64_t& val)
      {
         port.read(reinterpret_cast<uint8_t*>(&val), sizeof(uint64_t));
         little_endian(val);
         return *this;
      }
      
      byte_istream& operator>>(float& val)
      {
         port.read(reinterpret_cast<uint8_t*>(&val), sizeof(float));
         little_endian(val);
         return *this;
      }
      
      byte_istream& operator>>(double& val)
      {
         port.read(reinterpret_cast<uint8_t*>(&val), sizeof(double));
         little_endian(val);
         return *this;
      }
      
      byte_istream& operator>>(opaque& val)
      {
         port.read(val.data, val.size);
         return *this;
      }

   private:

      Port& port;
   };
}

#endif
