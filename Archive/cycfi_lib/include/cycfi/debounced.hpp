/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#if !defined(DEBOUNCED_HPP_OCTOBER_11_2012)
#define DEBOUNCED_HPP_OCTOBER_11_2012

#include <cycfi/pin.hpp>
#include <cycfi/constants.hpp>

namespace cycfi
{
   ////////////////////////////////////////////////////////////////////////////
   // Debounced switch
   ////////////////////////////////////////////////////////////////////////////
   template <int bit, int samples = 10>
   class debounced : private input_pin<bit>
   {
   public:

      debounced()
         : counter(0)
         , result(false)
         , prev(false)
      {}

      typedef input_pin<bit> base_type;

      bool on() const
      {
         if (input_pin<bit>::on())
         {
            if (counter == samples)
               result = true;
            else
               ++counter;
         }
         else
         {
            if (counter == 0)
               result = false;
            else
               --counter;
         }
         return result;
      }

      bool off() const
      {
         return !on();
      }

      operator bool() const
      {
         return on();
      }

      bool operator!() const
      {
         return off();
      }

      mode::edge_enum edge() const
      {
         bool curr = on();
         if (prev != curr)
         {
            prev = curr;
            return curr ? mode::rising : mode::falling;
         }
         return mode::no_edge;
      }

   private:

      mutable int counter : 6;
      mutable bool result : 1;
      mutable bool prev : 1;
   };
}

#endif
