/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#if !defined(UART_HPP_OCTOBER_8_2012)
#define UART_HPP_OCTOBER_8_2012

#include <stdint.h>

namespace cycfi
{
   class uart
   {
   public:

      uart(uint32_t baud);

      void write(uint8_t data);
      void write(uint8_t const* ptr, uint32_t len)
      {
         while (len--)
            write(*ptr++);
      }

      void read(uint8_t& data);
      void read(uint8_t* ptr, uint32_t len)
      {
         while (len--)
            read(*ptr++);
      }
   };
}

#endif
