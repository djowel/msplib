/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#if !defined(TOUCH_HPP_OCTOBER_8_2012)
#define TOUCH_HPP_OCTOBER_8_2012

#include <cycfi.hpp>
#include <cycfi/filter.hpp>
#include <cycfi/pin.hpp>

namespace cycfi
{
   ////////////////////////////////////////////////////////////////////////////
   // unfiltered (raw) touch sense class
   ////////////////////////////////////////////////////////////////////////////
   namespace detail
   {
      int32_t touch_impl();
   }
   
   template <int bit>
   struct touch_sense
   {
      int32_t operator()() const
      {
         input_pin<bit> a;
         peripheral_select<bit>(mode::pin_osc);
         int32_t result = detail::touch_impl();
         peripheral_select<bit>(mode::io_port);
         return result;
      }
   };

   //////////////////////////////////////////////////////////////////////////
   // low-pass filtered touch sense class
   //////////////////////////////////////////////////////////////////////////
   template <int bit, int k = 8>
   class filtered_touch_sense : touch_sense<bit>
   {
   public:

      static int const setup_loop = 1000;
      typedef touch_sense<bit> base_t;

      filtered_touch_sense()
         : baseline_(0)
      {
         //Get the baseline by running the filter many times
         for (int i = 0; i < setup_loop; ++i)
            filter(base_t::operator()());

         //set the baseline
         baseline_ = filter.current() / k;
      }

      int32_t operator()() const
      {
         int16_t value = base_t::operator()();
         return baseline_(filter(value) / k);
      }

      int32_t baseline() const
      {
         return baseline_();
      }

      void baseline(int32_t val)
      {
         baseline_ = val;
      }

   private:

      lowpass<k> filter;
      offset<> baseline_;
   };
}

#endif
