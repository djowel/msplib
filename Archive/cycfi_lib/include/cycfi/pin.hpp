/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#if !defined(PIN_HPP_OCTOBER_8_2012)
#define PIN_HPP_OCTOBER_8_2012

#include <stdint.h>
#include <cycfi/constants.hpp>

#ifdef N
#undef N
#endif

namespace cycfi
{
   ////////////////////////////////////////////////////////////////////////////
   // pin_base
   ////////////////////////////////////////////////////////////////////////////
   template <typename T, volatile T& ref, int bit>
   class pin_base
   {
   public:

      static T const mask = 1 << bit;

      bool on() const
      {
         return (ref & mask) != 0;
      }

      bool off() const
      {
         return !on();
      }

      operator bool() const
      {
         return on();
      }

      bool operator!() const
      {
         return off();
      }

      pin_base& operator=(bool val)
      {
         ref ^= (-val ^ ref) & mask;
         return *this;
      }
   };

   ////////////////////////////////////////////////////////////////////////////
   // output_port
   ////////////////////////////////////////////////////////////////////////////
   template <int N>
   struct output_pin;

   ////////////////////////////////////////////////////////////////////////////
   // input_port
   ////////////////////////////////////////////////////////////////////////////
   template <int N>
   struct input_pin;

   ////////////////////////////////////////////////////////////////////////////
   // peripheral_select
   ////////////////////////////////////////////////////////////////////////////   
   template <int N>
   void peripheral_select(mode::peripheral_enum select);
}

#endif
