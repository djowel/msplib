/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#if !defined(MEMREG_HPP_OCTOBER_8_2012)
#define MEMREG_HPP_OCTOBER_8_2012

#include <stdint.h>

namespace cycfi
{
   ////////////////////////////////////////////////////////////////////////////
   // Memory Mapped Registers
   ////////////////////////////////////////////////////////////////////////////

   // generic memory registers
   template <typename T, volatile T& ref>
   struct m_register
   {
      typedef volatile T value_type;
      operator value_type&() { return ref; }
      value_type& get() { return ref; }
   };

   // 8 bit memory registers
   template <volatile uint8_t& ref>
   struct m_register8 : m_register<uint8_t, ref> {};

   // 16 bit memory registers
   template <volatile uint16_t& ref>
   struct m_register16 : m_register<uint16_t, ref> {};

   // 32 bit memory registers
   template <volatile uint32_t& ref>
   struct m_register32 : m_register<uint32_t, ref> {};

   // 64 bit memory registers
   template <volatile uint64_t& ref>
   struct m_register64 : m_register<uint64_t, ref> {};
}

#endif
