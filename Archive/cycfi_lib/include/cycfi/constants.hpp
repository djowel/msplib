/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#if !defined(CONSTANTS_HPP_OCTOBER_20_2012)
#define CONSTANTS_HPP_OCTOBER_20_2012

namespace cycfi
{
   namespace mode
   {
      enum edge_enum
      {
         no_edge,
         rising,
         falling 
      };

      enum peripheral_enum
      {
         io_port,
         pin_osc
      };
   }
}

#endif
