/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#if !defined(ANALOG_HPP_OCTOBER_23_2012)
#define ANALOG_HPP_OCTOBER_23_2012

namespace cycfi
{
   namespace detail
   {
      void init_adc(int bit);
   }
   
   template<int bit>
   struct analog_pin
   {
   public:

      analog_pin()
      {
         detail::init_adc(bit);
      }
      
      uint16_t read();
   };
}
#endif

