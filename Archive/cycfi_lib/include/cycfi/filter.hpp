/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#if !defined(FILTER_HPP_OCTOBER_10_2012)
#define FILTER_HPP_OCTOBER_10_2012

#include <stdint.h>

namespace cycfi
{
   ////////////////////////////////////////////////////////////////////////////
   // Basic leaky-integrator filter. k will determine the effect of the
   // filter. Choose k to be a power of 2 for efficiency (the compiler
   // will optimize the comutation using shifts). k = 16 is a good starting
   // point.
   //
   // This simulates the RC filter in digital form. The equation is:
   //
   //    y[i] = rho * y[i-1] + s
   //
   // where rho < 1. To avoid floating point, we use k instead which
   // allows for integer operations. In terms of k, rho = 1 - (1 / k).
   // So the actual formula is:
   //
   //    y[i] += s - (y[i-1] / k);
   //
   // k will also be the filter gain, so the final result should be
   // divided by k.
   //
   ////////////////////////////////////////////////////////////////////////////
   template <int k, typename T = int>
   class lowpass
   {
   public:

      typedef T result_type;

      lowpass()
         : y(0) {}

      T operator()(T s) const
      {
         y += s - (y / k);
         return y;
      }

      T current() const { return y; }

   private:

      mutable T y;
   };

   ////////////////////////////////////////////////////////////////////////////
   // Peak detector. This filter will follow the envolope of a signal
   // with gradual decay (given by the decay parameter). The signal
   // decays exponentially if the signal is below the peak. Choose
   // decay to be a power of 2 for efficiency (the compiler will
   // optimize the comutation using shifts).
   ////////////////////////////////////////////////////////////////////////////
   template <int decay, typename T = int>
   class peak
   {
   public:

      typedef T result_type;

      peak()
         : y(0) {}

      T operator()(T s) const
      {
         if (s > y)
            y = s;
         else
            y -= (y-s) / decay;
         return y;
      }
      
      T current() const { return y; }

   private:

      mutable T y;
   };

   ////////////////////////////////////////////////////////////////////////////
   // Offset the signal a certain amount (e.g. to remove DC offset)
   ////////////////////////////////////////////////////////////////////////////
   template <typename T = int>
   class offset
   {
   public:

      typedef T result_type;

      offset(T baseline)
         : baseline(baseline) {}

      T operator()(T in) const
      {
         return in - baseline;
      }

      T operator()() const
      {
         return baseline;
      }

      offset& operator=(T baseline_)
      {
         baseline = baseline_;
         return *this;
      }

   private:

      T baseline;
   };

   ////////////////////////////////////////////////////////////////////////////
   // Clamp the signal to a maximum
   ////////////////////////////////////////////////////////////////////////////
   template <typename T = int>
   class clamp_max
   {
   public:

      typedef T result_type;

      clamp_max(T max)
         : max(max) {}

      T operator()(T in) const
      {
         if (in > max)
            return max;
         return in;
      }

      T operator()() const
      {
         return max;
      }

      clamp_max& operator=(T max_)
      {
         max = max_;
         return *this;
      }

   private:

      T max;
   };

   ////////////////////////////////////////////////////////////////////////////
   // Clamp the signal to a minimum
   ////////////////////////////////////////////////////////////////////////////
   template <typename T = int>
   class clamp_min
   {
   public:

      typedef T result_type;

      clamp_min(T min)
         : min(min) {}

      T operator()(T in) const
      {
         if (in < min)
            return min;
         return in;
      }

      T operator()() const
      {
         return min;
      }

      clamp_min& operator=(T min_)
      {
         min = min_;
         return *this;
      }

   private:

      T min;
   };
}

#endif
