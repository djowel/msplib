/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#if !defined(CYCFI_HPP_SEPTEMBER_18_2012)
#define CYCFI_HPP_SEPTEMBER_18_2012

#include <stdint.h>
#include <cstdlib>

#include <cycfi/memreg.hpp>
#include <cycfi/timing.hpp>
#include <cycfi/port.hpp>
#include <cycfi/pin.hpp>
#include <cycfi/task.hpp>

// Define for the MCU used
#define CYCFI_LIB_MCU MSP430

#if (CYCFI_LIB_MCU == MSP430)
# include <cycfi/detail/msp430/impl.hpp>
#endif

#endif
