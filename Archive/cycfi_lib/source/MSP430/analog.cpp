/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#include <cycfi.hpp>
#include <stdint.h>
#include <cycfi/analog.hpp>

namespace cycfi
{
   namespace detail
   {
      void init_adc(int bit)
      {
         ADC10CTL0 &= ~ENC;
         ADC10CTL1 = (bit*0x1000u);
         ADC10AE0 = 1<<bit;
         ADC10CTL0 = ADC10SHT_2 + ADC10ON + ENC;
      }
                  
      uint16_t read()
      {
         ADC10CTL0 |= ADC10SC;
         while(ADC10CTL1 & ADC10BUSY)
         uint16_t adc_value = ADC10MEM;
         return adc_value;
      }
   }
}