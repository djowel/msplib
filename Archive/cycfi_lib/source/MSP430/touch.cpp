/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#include<cycfi/touch.hpp>

#if !defined(CYCFI_WDT_INTERVAL)
#define CYCFI_WDT_INTERVAL WDT_MDLY_0_5
#endif

#if defined(CYCFI_WDT_INTERVAL_64)
#define  CYCFI_WDT_INTERVAL WDT_MDLY_0_064
#endif

#if defined(CYCFI_WDT_INTERVAL_512)
#define CYCFI_WDT_INTERVAL WDT_MDLY_0_5
#endif

#if defined(CYCFI_WDT_INTERVAL_8192)
#define CYCFI_WDT_INTERVAL WDT_MDLY_8
#endif

#if defined(CYCFI_WDT_INTERVAL_32768)
#define CYCFI_WDT_INTERVAL WDT_MDLY_32
#endif

namespace cycfi
{
   uint32_t const wdt_interval = CYCFI_WDT_INTERVAL;
   
   namespace detail
   {     
      int32_t touch_impl()
      {
         TA1CCTL0 &= ~CCIE;
         TA1CCTL1 &= ~CCIE;
         TA1CCTL2 &= ~CCIE;

         TA0CTL = TASSEL_3 + MC_2;                   // TACLK, cont mode
         TA0CCTL1 = CM_3 + CCIS_2 + CAP;             // Pos&Neg,GND,Cap

         // Setup Gate Timer
         WDTCTL = wdt_interval;                      // WDT, ACLK, interval timer
         TA0CTL |= TACLR;                            // Clear Timer_A TAR

         __bis_SR_register(LPM0_bits + GIE);           // Wait for WDT interrupt
         TA0CCTL1 ^= CCIS0;                          // Create SW capture of CCR1
         int32_t meas_cnt = TACCR1;                  // Save result
         WDTCTL = WDTPW + WDTHOLD;                   // Stop watchdog timer

         TA1CCTL0 |= CCIE;
         TA1CCTL1 |= CCIE;
         TA1CCTL2 |= CCIE;

         return  -(meas_cnt);
      }
   }
}
