/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#include <cycfi.hpp>

namespace cycfi
{
   class my_interrupt1 : public pin_interrupt<1>
   {
   public:

      my_interrupt1()
         : base_type(4), led(mode::out){}

      virtual bool on_edge(uint32_t time)
      {
         led = !led;
         delay_ms(500);
         return false;
      }

   private:

      port_a0 led;
   };


   class my_interrupt2 : public pin_interrupt<1>
   {
   public:

      my_interrupt2()
         : base_type(5), led(mode::out){}

      virtual bool on_edge(uint32_t time)
      {
         led = !led;
         delay_ms(500);
         return false;
      }

   private:

      port_a6 led;
   };

   my_interrupt1 myint1;
   my_interrupt2 myint2;

   void main()
   {
      myint1.enable(mode::rising);
      myint2.enable(mode::rising);
   }
}

#include <cycfi_main_impl.hpp>

