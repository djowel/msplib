/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#include <cycfi.hpp>
#include <cycfi/uart.hpp>
#include <cycfi/byte_ostream.hpp>
#include <cycfi/midi.hpp>
#include <cycfi/debounced.hpp>



namespace cycfi
{
   uart port(31250);
   byte_ostream<uart>out(port);

   void key_note(bool val, uint8_t ch, uint8_t note, uint8_t vel)
   {
      bool state = false;
      bool set = false;
      
      if (val)
      {
         if (!set)
            state = !state;
         set = true;
      }

      else
      {
         set = false;
      }

      if(state)      
         out << midi::note_on(ch, note, vel);
   
      else
         out << midi::note_off(ch, note, vel);

      delay_ms(10);
   }

   void main()
   {
      debounced<3> key_1;
      debounced<4> key_2;
      debounced<5> key_3;
      debounced<8> key_4;

      while (1)
      {
         key_note(key_1.on(), 1, 60, 127);                        //C
         key_note(key_2.on(), 1, 62, 127);                        //D
         key_note(key_3.on(), 1, 64, 127);                        //E
         key_note(key_4.on(), 1, 65, 127);                        //F
         
         //TODO: add more keys (lack of pushbutton)
      }
   }
}

#include <cycfi_main_impl.hpp>
