#include <cycfi.hpp>
#include <cycfi/uart.hpp>
#include <cycfi/byte_ostream.hpp>
#include <cycfi/debounced.hpp>

namespace cycfi
{
   uart uart_port(9600);
   byte_ostream<uart> out(uart_port);

   void print_values(int16_t a, int16_t b, int16_t c, int16_t d)
   {
      out
         << (int16_t) 0xCDAB
         << (int16_t) 8
         << a
         << b
         << c
         << d
            ;
   }
   
   void main()
   {
      debounced<3> sw1;
      bool led_state = false;
      bool set = false;
      output_pin<6> led;
      led = 0;

      while (1)
      {

         if (sw1.on())
         {
            if (!set)
               led_state = !led_state;
            set = true;
         }

         else
         {
            set = false;
         }

         if(led_state)
            led = 1;

         else
            led = 0;

         delay_ms(10);
      }
   }
}

#include <cycfi_main_impl.hpp>
