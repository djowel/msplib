/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#include <cycfi.hpp>

namespace cycfi
{
   template <>
   struct task<1> : task_base<1>
   {
      static uint16_t const period = 1000;

      task() : i(0)
      {
         led = 1;
      }

      void perform()
      {
         if (i++ == 250)
         {
            led = !led;
            i = 0;
         }
      }

      output_pin<0> led;  // port 1.0
      int i;
   };

   template <>
   struct task<2> : task_base<2>
   {
      static uint16_t const period = 1000;

      task() : i(0)
      {
         led = 1;
      }

      void perform()
      {
         if (i++ == 1000)
         {
            led = !led;
            i = 0;
         }
      }

      output_pin<1> led;  // port 1.1
      int i;
   };

   template <>
   struct task<3> : task_base<3>
   {
      static uint16_t const period = 1000;

      task() : i(0)
      {
        led = 1;
      }

      void perform()
      {
         if (i++ == 500)
         {
            led = !led;
            i = 0;
         }
      }

      output_pin<2> led;  // port 1.2
      int i;
   };
   
   template <>
   struct task<4> : task_base<4>
   {
      static uint16_t const period = 1000;

      task() : i(0)
      {
        led = 1;
      }

      void perform()
      {
         if (i++ == 100)
         {
            led = !led;
            i = 0;
         }
      }

      output_pin<3> led;  // port 1.3
      int i;
   };
   
   void main()
   {
      sleep();
   }
}

#include <cycfi_main_impl.hpp>
