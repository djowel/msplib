/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#include <cycfi.hpp>
#include <cycfi/touch.hpp>
#include <cycfi/filter.hpp>
#include <cycfi/uart.hpp>
#include <cycfi/byte_ostream.hpp>

namespace cycfi
{
   uart uart_port(9600);
   byte_ostream<uart> out(uart_port);

   void print_values(int16_t a, int16_t b, int16_t c, int16_t d)
   {
      out
         << (int16_t) 0xCDAB           // magic
         << (int16_t) 8                // payload
         << a                          // channel 1
         << b                          // channel 2
         << c                          // channel 3
         << d                          // channel 4
         ;
   }

   void main()
   {
      filtered_touch_sense<12> pad;    

      int out_interval = 10;

      while (1)
      {
         if (out_interval--)
         {
            print_values(pad(), pad.baseline(), 0, 0);
            out_interval = 10;
         }
         delay_ms(1);
      }
   }
}

#include <cycfi_main_impl.hpp>

