/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#include <cycfi.hpp>
#include <cycfi/uart.hpp>

namespace cycfi
{
   void main()
   {
      uart uart(2400);

      while (1)
      {
         uart.write('J');
         delay_ms(100);
      }
   }
}

#include <cycfi_main_impl.hpp>


