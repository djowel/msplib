/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/

#include <cycfi.hpp>

namespace cycfi
{
   template <>
   struct task<1> : task_base<1>
   {
      static uint16_t const period = 1000;

      task() : i(0)
      {
         led = 1;
      }

      void perform()
      {
         if (i++ == 500)
         {
            led = !led;
            i = 0;
         }
      }

      int i;
      output_pin<6> led;
   };

   void main()
   {
      sleep();
   }
}

#include <cycfi_main_impl.hpp>
