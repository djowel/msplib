/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#include <cycfi.hpp>
#include <cycfi/uart.hpp>
#include <cycfi/byte_ostream.hpp>

namespace cycfi
{
   void main()
   {
      uart port(9600);
      byte_ostream<uart> out(port);

      while (1)
      {
         out << 'J' << 'e' << 'w' << 'a' << 'r' << 'd' << '\n';
         out << "Jeward\n";
         out << (uint16_t) 0xABCD;
         out << (uint32_t) 0xDEADBEEF;
         out << (uint64_t) 0xA0B0C0D0AFBFCFDF;
         out << (float) 1.0;
         out << (double) 1.0;

         out << '\n';
         delay_ms(100);
      }
   }
}

#include <cycfi_main_impl.hpp>


