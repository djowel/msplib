/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#include <cycfi.hpp>
#include <cycfi/touch.hpp>
#include <cycfi/uart.hpp>
#include <cycfi/byte_ostream.hpp>
#include <cycfi/debounced.hpp>

namespace cycfi
{
   uart uart_port(9600);
   byte_ostream<uart> out(uart_port);

   void print_values(int16_t a, int16_t b, int16_t c, int16_t d)
   {
      out 
         << (int16_t) 0xCDAB
         << (int16_t) 8
         << a
         << b
         << c
         << d
         ;
   }

   void main()
   {
      touch_sense<4> t1;
      touch_sense<5> t2;
      touch_sense<6> t3;
      touch_sense<7> t4;
      touch_sense<8> t5;
      touch_sense<9> t6;
      touch_sense<12> t7;
      touch_sense<13> t8;

      debounced<10> sw1;
      bool first = false;
      bool set = false;
      output_pin<0> led;
      led = 0;
      
      while (1)
      {
         if (sw1.on())
         {
            if (!set)
               first = !first;
            set = true;
         }
         
         else
         {
            set = false;
         }

         if (first)
         {
            print_values(t1(), t2(), t3(), t4());
            led = 1;
         }
         
         else
         {
            print_values(t5(), t6(), t7(), t8());
            led = 0;
         }
         
         delay_ms(3);
      }
   }
}

#include <cycfi_main_impl.hpp>

