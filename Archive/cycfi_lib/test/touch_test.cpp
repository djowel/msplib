/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#include <cycfi.hpp>
#include <cycfi/touch.hpp>
#include <cycfi/uart.hpp>
#include <cycfi/byte_ostream.hpp>

namespace cycfi
{
   uart uart_port(9600);
   byte_ostream<uart> out(uart_port);

   void print_values(int16_t a, int16_t b, int16_t c, int16_t d)
   {
      out
         << (int16_t) 0xCDAB           // magic
         << (int16_t) 8                // payload
         << a                          // channel 1
         << b                          // channel 2
         << c                          // channel 3
         << d                          // channel 4
         ;
   }

   void main()
   {
      touch_sense<4> t1;
      touch_sense<5> t2;

      while (1)
      {
         print_values(t1(), t2(), 0, 0);
         delay_ms(3);
      }
   }
}

#include <cycfi_main_impl.hpp>

