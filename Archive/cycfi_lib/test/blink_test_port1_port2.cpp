/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/

#include <cycfi.hpp>

namespace cycfi
{
   template <>
   struct task<1> : task_base<1>
   {
      static uint16_t const period = 1000;

      task() : i(0)
      {
    	  port1_led = 1;

      }

      void perform()
      {
         if (i++ == 500)
         {
        	 port1_led = !port1_led;
        	 port2_led = !port1_led;
            i = 0;
         }
      }

      int i;
      output_pin<6> port1_led;
      output_pin<13> port2_led;
   };

   void main()
   {
      sleep();
   }
}

#include <cycfi_main_impl.hpp>
