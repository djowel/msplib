/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#include <cycfi.hpp>
#include <cycfi/touch.hpp>
#include <cycfi/uart.hpp>
#include <cycfi/byte_ostream.hpp>
//~ #include <cstdlib>

namespace cycfi
{
   uart uart_port(9600);
   byte_ostream<uart> out(uart_port);

   void print_values(int16_t a, int16_t b, int16_t c, int16_t d)
   {
      out
         << (int16_t) 0xCDAB           // magic
         << (int16_t) 8                // payload
         << a                          // channel 1
         << b                          // channel 2
         << c                          // channel 3
         << d                          // channel 4
         ;
   }

   void print_digit(uint32_t val)
   {
      char buff[16];
      std::ltoa(val, buff);
      out << (char const*) buff;
   }

   ////////////////////////////////////////////////////////////////////////////
   int16_t slider_value(
      int16_t const values[]
    , unsigned size
    , int16_t threshold)
   {
      int32_t a = 0;
      int32_t b = 0;

      for (int i = size-1; i >= 0; --i)
      {
         int16_t value = values[i];
         if (value < threshold)
         {
            if (a)   // if we got at least one value, we break from the loop
               break;
         }
         else
         {
            a += value;
            b += (i + 1) * value;
         }
      }

      // If we got no value, return -1
      if (a == 0)
         return -1;

      b /= (a / 16);
      b -= 16;
      if (b < 0)
          b = 0;

      return b;
   }

   ////////////////////////////////////////////////////////////////////////////
   class slider
   {
   public:

      slider(int threshold)
         : threshold(threshold)
      {
      }

      int16_t operator()()
      {
         // scan all pads and put the results in the values array
         values[0] = p1();
         values[1] = p2();
         values[2] = p3();
         values[3] = p4();
         values[4] = p5();
         values[5] = p6();
         values[6] = p7();
         values[7] = p8();

//         for(int i = 0; i < 8; i++)
//         {
//        	 print_digit(values[i]);
//        	 out << ",";
//         }

         // compute the slider value
         return slider_value(values, 8, threshold);
      }

   private:

      filtered_touch_sense<4> p1;
      filtered_touch_sense<5> p2;
      filtered_touch_sense<6> p3;
      filtered_touch_sense<7> p4;
      filtered_touch_sense<8> p5;
      filtered_touch_sense<9> p6;
      filtered_touch_sense<12> p7;
      filtered_touch_sense<13> p8;
      int16_t values[8];
      int threshold;
   };

   ////////////////////////////////////////////////////////////////////////////
   void main()
   {
      slider s(15);
      int out_interval = 10;

      int const k = 8;
      lowpass<k> filter;

      clamp_max<> max = 800;
      clamp_min<> min = 0;

      while (1)
      {
         int16_t value = min(max(s()));
         int16_t filtered = filter(value) / k;
         if (out_interval--)
         {
//             out << ", value = ";
//             print_digit(value);
//             out << '\n';
        	   print_values(filtered, 0, 0, 0);
        	   out_interval = 10;
         }
         delay_ms(1);
      }
   }
}

#include <cycfi_main_impl.hpp>

