/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#include <cycfi.hpp>
#include <cycfi/touch.hpp>
#include <cycfi/filter.hpp>
#include <cycfi/uart.hpp>
#include <cycfi/byte_ostream.hpp>

namespace cycfi
{
   uart uart_port(9600);
   byte_ostream<uart> out(uart_port);

   void print_values(int16_t a, int16_t b, int16_t c, int16_t d)
   {
      out
         << (int16_t) 0xCDAB           // magic
         << (int16_t) 8                // payload
         << a                          // channel 1
         << b                          // channel 2
         << c                          // channel 3
         << d                          // channel 4
         ;
   }

   void main()
   {
      touch_sense<4>pad;        
      
      // k = 16 (see filter.hpp lowpass class)
      int const k = 8;

      // A lowpass filter
      lowpass<k> filter;

      // Get the baseline by running the filter many times
      for (int i = 0; i < 1000; ++i)
         filter(pad());

      // The baselene will be the current value of the filter
      // divided by the filter gain.
      offset<> baseline(filter.current() / k);

      clamp_max<> max(800);
      clamp_min<> min(baseline());

      int out_interval = 10;

      while (1)
      {
         int16_t value = min(max(pad()));
         int16_t filtered = filter(baseline(value)) / k;

         if (out_interval--)
         {
            print_values(value, filtered, baseline(), 0);
            out_interval = 10;
         }
         delay_ms(1);
      }
   }
}

#include <cycfi_main_impl.hpp>

