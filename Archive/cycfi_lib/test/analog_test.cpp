/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#include <cycfi.hpp>
#include<cycfi/analog.hpp>

namespace cycfi
{
	void main()
	{
		analog_pin<1> ch_1;
		output_pin<0> led;
		led = 0;

		while(1)
		{
			if(ch_1.read() > 50)
				led = 1;
			else
				led = 0;
		}
	}
}

#include <cycfi_main_impl.hpp>
