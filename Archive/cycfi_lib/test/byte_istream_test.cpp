/*=============================================================================
Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#include <cycfi.hpp>
#include <cycfi/uart.hpp>
#include <cycfi/byte_ostream.hpp>
#include <cycfi/byte_istream.hpp>

namespace cycfi
{
   void main()
   {
      uart port(9600);
      byte_ostream<uart> out(port);
      byte_istream<uart> in(port);
      char a;

      while (1)
      {
         in >> a;
         out << a;
         delay_ms(100);
      }
   }
}

#include <cycfi_main_impl.hpp>


