/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#include <cycfi.hpp>
#include <cycfi/touch.hpp>
#include <cycfi/uart.hpp>
#include <cycfi/byte_ostream.hpp>

namespace cycfi
{
   uart uart_port(9600);
   byte_ostream<uart> out(uart_port);

   void print_values(int16_t a, int16_t b, int16_t c, int16_t d)
   {
      out 
         << (int16_t) 0xCDAB
         << (int16_t) 8
         << a
         << b
         << c
         << d
         ;
   }

   void main()
   {
	   filtered_touch_sense<4> t1;
	   filtered_touch_sense<5> t2;
	   filtered_touch_sense<12> t3;
	   filtered_touch_sense<13> t4;

      while (1)
      {
         print_values(t1(), t2(), t3(), t4());
         delay_ms(3);
      }
   }
}

#include <cycfi_main_impl.hpp>

