/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#include <cycfi.hpp>
#include <cycfi/touch.hpp>
#include <cycfi/uart.hpp>
#include <cycfi/byte_ostream.hpp>

namespace cycfi
{
   template <>
   struct task<4> : task_base<4>
   {
      static uint16_t const period = 1000;

      task() : i(0)
      {
         led = 1;
      }

      void perform()
      {
         if (i++ == 500)
         {
            led = !led;
            i = 0;
         }
      }

      output_pin<0> led;  // port 1.0
      int i;
   };

   template <>
   struct task<5> : task_base<5>
   {
      static uint16_t const period = 1000;

      task() : i(0)
      {
         led = 1;
      }

      void perform()
      {
         if (i++ == 1000)
         {
            led = !led;
            i = 0;
         }
      }

      output_pin<6> led;  // port 1.6
      int i;
   };

   uart uart_port(9600);
   byte_ostream<uart> out(uart_port);

      void print_values(int16_t a, int16_t b, int16_t c, int16_t d)
      {
        out
          << (int16_t) 0xCDAB           // magic
          << (int16_t) 8                // payload
          << a                          // channel 1
          << b                          // channel 2
          << c                          // channel 3
          << d                          // channel 4
          ;
      }

   touch_sense<12> p1;

   void main()
   {
      int out_interval = 10;
      while (1)
      {
         if (out_interval--)
         {
            print_values(p1(), 0, 0, 0);
            out_interval = 10;
         }
         delay_ms(1);
      }
   }
}

#include <cycfi_main_impl.hpp>
