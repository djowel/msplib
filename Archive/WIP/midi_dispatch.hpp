/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#if !defined(PIN_HPP_OCTOBER_8_2012)
#define PIN_HPP_OCTOBER_8_2012

#include <cycfi/midi.hpp>

namespace cycfi { namespace midi
{
   ////////////////////////////////////////////////////////////////////////////
   // dispatch_midi
   //
   //    Prior to call, the caller must read the status_byte from the
   //    byte_istream. F f is a polymorphic function object. For valid
   //    MIDI messages, f is called with the properly constructed MIDI
   //    message object (see midi.hpp).
   //
   //    dispatch_midi returns non-zero if there's an error (e.g. invalid
   //    status byte). In such a case, the caller can read bytes from the
   //    stream until the next status byte.
   //
   ////////////////////////////////////////////////////////////////////////////
   template <typename F>
   int dispatch_midi(uint8_t status_byte, byte_istream& in, F f)
   {
      if (status_byte == status::sysex)
      {
         uint8_t data;
         in >> data;
         while (data != status::sysex_end)
            in >> data;

         // $$$ TODO: do something about the sysex data read $$$
         return 0;
      }

      uint8_t channel_msg = status_byte & 0xF0;
      uint8_t channel = status_byte & 0x0F;
      uint8_t first;
      uint8_t second;

      // message size == 2
      if (channel_msg == status::program_change ||
         channel_msg == status::channel_aftertouch ||
         status_byte == status::song_select)
      {
         in >> first;
      }

      // message size == 3
      else if (status_byte < status::tune_request)
      {
         in >> first;
         in >> second;
      }

      switch (channel_msg)
      {
         case status::note_off:
            f(note_off(channel, first, second));
            break;

         case status::note_on:
            f(note_on(channel, first, second));
            break;

         case status::poly_aftertouch:
            f(poly_aftertouch(channel, first, second));
            break;

         case status::control_change:
            f(control_change(
               channel, cc::controller(first), second));
            break;

         case status::pitch_bend:
            f(pitch_bend(channel, first, second));
            break;

         case status::program_change:
            f(program_change(channel, first));
            break;

         case status::channel_aftertouch:
            f(channel_aftertouch(channel, first));
            break;

         default:
            switch (status_byte)
            {
               case status::song_position:
                  in >> first;
                  in >> second;
                  f(song_position(first, second));
                  break;

               case status::song_select:
                  in >> first;
                  f(song_select(first));

               case status::tune_request:
                  f(tune_request());
                  break;

               case status::timing_tick:
                  f(timing_tick());
                  break;

               case status::start:
                  f(start());
                  break;

               case status::continue_:
                  f(continue_());
                  break;

               case status::stop:
                  f(stop());
                  break;

               case status::active_senseing:
                  f(active_senseing());
                  break;

               case status::reset:
                  f(reset());
                  break;

               default:
                  // $$$ Error! $$$
                  return -1;
            }
      }
      return 0;
   }
}}

#endif
