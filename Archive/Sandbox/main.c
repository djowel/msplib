/*
 * main.c
 */
#define RXD BIT1
#define TXD BIT2
#include <msp430g2553.h>




void main(void)
{
	WDTCTL  = WDTPW + WDTHOLD;
	BCSCTL1 = CALBC1_1MHZ;
	DCOCTL = CALDCO_1MHZ;

	P1SEL  = RXD + TXD;
  	P1SEL2 = RXD + TXD;
  	UCA0CTL1 |= UCSSEL_2;                     // SMCLK
  	UCA0BR0 = 104;                            // 1MHz 9600
  	UCA0BR1 = 0;                              // 1MHz 9600
  	UCA0MCTL = UCBRS0;                        // Modulation UCBRSx = 1
  	UCA0CTL1 &= ~UCSWRST;                     // Initialize USCI state machine
  	IE2 |= UCA0RXIE + UCA0TXIE;				  // Enable RX and TX interrupt vectors

  	 __bis_SR_register(GIE);

}

#pragma vector = USCIAB0RX_VECTOR;
__interrupt void USCIA0RX_ISR (void)
{
	while (!(IFG2&UCA0RXIFG));                // USCI_A0 RX buffer ready?
		UCA0TXBUF =  UCA0RXBUF;				  // Echo the value typed
}

#pragma vector = USCIAB0TX_VECTOR;
__interrupt void USCIA0TX_ISR (void)
{
	while (!(IFG2&UCA0TXIFG));                // USCI_A0 TX buffer ready?
		UCA0TXBUF =  'B';					  //print B in the port
}
