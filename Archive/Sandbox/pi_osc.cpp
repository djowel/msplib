#include <msp430g2553.h>
#include <cstdlib>
#include <stdint.h>
#include<uart.hpp>

using namespace cycfi;


uint32_t timer_count = 0;
uint32_t prev_count = 0;
void delay_ms(uint32_t ms)
{
	while(--ms)
		_delay_cycles(1000000/1000);
}
int main()
{
  WDTCTL = WDTPW + WDTHOLD;               
  BCSCTL1 = CALBC1_1MHZ;                 
  DCOCTL =  CALDCO_1MHZ; 
  TACTL = MC_2 + TASSEL_2;

  CCR0 = 0;
  CCTL0 = CCIE;

  uart_setup(9600);

  P1DIR |= 0x01;
  
  P1IE |=BIT4;
  P1IES |= BIT4;
  P1IFG &= ~BIT4;



//	P1DIR &= ~ BIT4;                        // P1.1 is the input used here
//	P1SEL &= ~ BIT4;
//	P1SEL2 |= BIT4;



	  _EINT();

  
  while(1)
  {
	  print_digit(prev_count);
      delay_ms(50);
  }
  
}

#pragma vector = TIMER0_A0_VECTOR
__interrupt void timer0_ISR()
{
	CCR0 += 1000;
}

#pragma vector = PORT1_VECTOR
__interrupt void port_ISR()
{
   timer_count = TAR - timer_count;
   prev_count = (timer_count + prev_count)/2;
   P1OUT ^= 0x01;
   P1IFG &= ~BIT4;
}


