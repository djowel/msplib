#include <stdint.h>
#include <msp430.h>

//command bytes for LCD
#define CMD_CLR   0x01
#define CMD_RIGHT 0x1C
#define CMD_LEFT  0x18
#define CMD_HOME  0x02

// bitmasks for control bits on shift register
#define SHIFT_EN 0x10;
#define SHIFT_RW 0x20;
#define SHIFT_RS 0x40;


namespace cycfi
{
   uint8_t data_pin;
   uint8_t latch_pin;
   uint8_t clock_pin;
   int lcd_lines;
   
   void delay_interval(uint32_t ms)
   {
      while(--ms)
         _delay_cycles(1000 / 1000);
   }
   
   void pin_write(uint16_t bit, uint8_t val)
   {
      if(val)
         P1OUT |= bit;
      else
         P1OUT &= ~bit;
   }
   void pulse_clock()
   {
      P1OUT |= clock_pin;
      P1OUT ^= clock_pin;
   }

   void shift_out(uint8_t val)
   {
      P1OUT &= ~latch_pin;
      int8_t i;

        for (i = 0; i < 8; i++)
        {
           pin_write(data_pin, (val & (1 << i)));
           pulse_clock();
        }

        P1OUT |= latch_pin;
        P1OUT &= ~latch_pin;
   }

   void _pushOut(uint8_t val)
   {
      shift_out(val);
      P1OUT |= latch_pin;
      delay_interval(10);
      P1OUT &= ~latch_pin;
   }

   void _pushNibble(int nibble, bool cmd)
   {
      if (!cmd)
      {
         nibble |= SHIFT_RS; // set DI HIGH
         nibble &= ~SHIFT_RW; // set RW LOW
      }

      nibble &= ~SHIFT_EN; // set Enable LOW
      _pushOut(nibble);
      nibble |= SHIFT_EN; // Set Enable HIGH
      _pushOut(nibble);
      nibble &= ~SHIFT_EN; // set Enable LOW
      _pushOut(nibble);
   }

   void _pushByte(int val, bool cmd)
   {
      int nibble = 0;

      P1OUT &= ~latch_pin;

      nibble = val >> 4; //send the first 4 databits (from 8)
      _pushNibble(nibble, cmd);

      delay_interval(1);

      nibble = val & 15; // set HByte to zero
      _pushNibble(nibble, cmd);
   }

   void command_write(uint8_t val)
   {
      _pushByte(val, true);
      delay_interval(5);
   }
   
   void init_LCD(int clock, int data, int latch, int lines)
   {
      lcd_lines = lines;
      data_pin = 1 << data;
      latch_pin = 1 << latch;
      clock_pin = 1 << clock;
           
      if (lcd_lines < 1 || lcd_lines > 2)
         lcd_lines = 1;
         
      P1DIR |= (data_pin + clock_pin + latch_pin);
      delay_interval(100);
      
      command_write(0x03); // function set: 4 pin initialization
      command_write(0x03); // function set: 4 pin initialization
      command_write(0x03); // function set: 4 pin initialization
      command_write(0x02); // function set: 4 pin initialization

      if (lcd_lines==1)
         command_write(0x20); // function set: 4-bit interface, 1 display line, 5x7 font
      else
          command_write(0x28); // function set: 4-bit interface, 2 display lines, 5x7 font
      /////////// end of 4 pin initialization

      command_write(0x06); // entry mode set:
           // increment automatically, no display shift
      command_write(0x0c); // display control:
           // turn display on, cursor on, no blinking
      command_write(0x01);
           // clear display, set cursor position to zero
   }
   
   void print(uint16_t val)
   {
      _pushByte(val, false);
      delay_interval(5);
   }

   void printIn(char msg[])
   {
      int i;
      for (i=0;i < strlen(msg);i++)
         print(msg[i]);
   }

   void clear()
   {
      command_write(CMD_CLR);
   }

   void set_cursor(int line_num, int x)
   {
      int z;
      //first, put cursor home
      command_write(CMD_HOME);

      //if we are on a 1-line display, set line_num to 1st line, regardless of given
      if (lcd_lines==1)
         line_num = 1;
      //offset 40 chars in if second line requested
      if (line_num == 2)
          x += 40;

       //advance the cursor to the right according to position. (second line starts at position 40).
      for (z = 0; z < x; z++)
         command_write(0x14);
   }

   void leftScroll(int num_chars, int delay_time)
   {
      int i;
      for	(i=0; i<num_chars; i++)
      {
         command_write(CMD_LEFT);
         delay_interval(delay_time);
      }
   }   

}


