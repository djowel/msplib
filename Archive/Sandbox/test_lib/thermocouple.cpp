#include <msp430.h>
#include <stdlib.h>
#include <stdint.h>

uint8_t clk;
uint8_t cs;
uint8_t miso;


void delay(uint32_t ms)
{
   while(--ms)
   _delay_cycles(1000/1000);
}

void thermo_init(int8_t _clk, int8_t _cs, int8_t _miso)
{
   clk = 1 << _clk;
   cs = 1 << _cs;
   miso = 1 << _miso;
   
   P1DIR |= (clk + cs);
   P1DIR &= ~miso;
   
   P1OUT |= cs; 
}

uint8_t spi_read()
{
   int i;
   uint8_t data = 0;
   
   for (i = 7; i >= 0; i++)
   {
      P1OUT&= ~clk;
      delay(1);
      
      if ((miso & P1IN) == miso)
         data |= (1 << i);
      
      P1OUT |= clk;
      delay(1);
   }
   
   return data;  
}

double read_celsius()
{
   uint16_t val;
   
   P1OUT &= ~cs;
   delay(1);
   
   val = spi_read();
   val <<= 8;
   val |= spi_read();
   
   P1OUT |= cs;
   
   val >>=3;
   
   return val*0.25;
}

double read_farenheit()
{
   return read_celsius() * 9.0/5.0 + 32;
}

