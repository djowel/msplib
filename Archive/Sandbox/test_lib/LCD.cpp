/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#include <LCD.hpp>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>

namespace cycfi
{
      namespace detail
      {

         void delay_interval(uint32_t ms)
         {
            while(--ms)
               _delay_cycles(1000 / 1000);
         }

         void init_LCD(int _clock_pin, int _data_pin, int _latch_pin , int _lcd_lines)
         {
            lcd_lines = _lcd_lines;
            data_pin = 1 << _data_pin;
            latch_pin = 1 << _latch_pin;
            clock_pin = 1 << clock_pin;

            if (lcd_lines < 1 || lcd_lines > 2)
            lcd_lines = 1;

            P1DIR |= (data_pin + clock_pin + latch_pin);
            delay_interval(100);

         }


      }
}
