/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#if !defined(PIN_OSC_HPP_OCTOBER_18_2012)
#define PIN_OSC_HPP_OCTOBER_18_2012

namespace cycfi
{ 
   template<int ID>
   class pin_osc
   {
   
   public:
   
      pin_osc(int bit_in_)
      {
         set(bit_in_);
      }
      
      void set(int bit_in_)
      {
         bit_in = 1<<bit_in_;
      }
      
      int32_t measure_count()
      {
         TA0CTL = TASSEL_3 + MC_2;                   // TACLK, cont mode
         TA0CCTL1 = CM_3 + CCIS_2 + CAP;               // Pos&Neg,GND,Cap
      
         P2DIR &= ~ bit_in;
         P2SEL &= ~ bit_in;
         P2SEL2 |= bit_in;
      
         /*Setup Gate Timer*/
         WDTCTL = WDT_MDLY_0_5;              // WDT, ACLK, interval timer
         TA0CTL |= TACLR;                        // Clear Timer_A TAR
         __bis_SR_register(LPM0_bits+GIE);       // Wait for WDT interrupt
         TA0CCTL1 ^= CCIS0;                      // Create SW capture of CCR1
         int32_t meas_cnt = TACCR1;                      // Save result
         WDTCTL = WDTPW + WDTHOLD;               // Stop watchdog timer
         P2SEL2 &= ~bit_in;
      
         return meas_cnt;
      }
   
   private:
   
      int bit_in;
   
   };
}  

#endif