#ifndef LCD_H_
#define LCD_H_

#define sendData(data) send(data, 1)
#define sendInstruction(data) send(data, 0)
#define initDisplay() sendInstruction(0x3C); sendInstruction(0x0C); clearDisplay(); sendInstruction(0x06)
#define clearDisplay() sendInstruction(0x01); _delay_cycles(2000)
#define second_line() sendInstruction((0x80 | 0x40)); _delay_cycles(2000)
#define DATAPIN BIT0
#define CLOCKPIN BIT1
#define ENABLEPIN BIT2


void send(char data, char registerSelect);
void sendDataArray(char data[], char length);
void Print_Screen(char *texts);

char charIndex = 0;
char bitCounter = 0;

void sendDataArray(char data[], char length) {
   charIndex = 0;
     while(charIndex < length) {
        sendData(data[charIndex]);
        charIndex++;
     }
}

void send(char data, char registerSelect) {
   bitCounter = 0;
   while(bitCounter < 8) {
        (data & BIT7) ? (P1OUT |= DATAPIN) : (P1OUT &= ~DATAPIN);
        data <<= 1;
        P1OUT |= CLOCKPIN;
        P1OUT &= ~CLOCKPIN;
        bitCounter++;
     }
     registerSelect ? (P1OUT |= DATAPIN) : (P1OUT &= ~DATAPIN);
     P1OUT &= ~ENABLEPIN;
     P1OUT |= ENABLEPIN;
}


void Print_Screen(char *texts)
{
volatile int i=0;	
char *h;
h=texts;
for(i=0;i<16;i++)
{
if(h[i]==0)
	{
	break;	
	}
}
sendDataArray(h, i);	
}

#endif /*LCD_H_*/