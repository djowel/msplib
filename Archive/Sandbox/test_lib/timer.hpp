/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#if !defined(TIMER_HPP_SEPTEMBER_28_2012)
#define TIMER_HPP_SEPTEMBER_28_2012

namespace cycfi
{ 
      
   struct stop_watch
   {
      inline uint16_t sys_tick()
      {
         return TAR;
      }   
  
      stop_watch()
      {
         start();
      }
         
      void start()
      {
         start_time = sys_tick(); 
      }
         
      uint16_t elapsed_time()
      {
         return sys_tick() - start_time;
      }
         
      uint16_t start_time;
         
      };
      
  
}

#endif