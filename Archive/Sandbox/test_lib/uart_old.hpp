/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#include <msp430.h>
#include <stdint.h>
#include <cstdlib>

namespace cycfi
{ 
   void hardware_uart_setup()  
   {
      P1SEL  = BIT1 + BIT2;
      P1SEL2 = BIT1 + BIT2;
      UCA0CTL1 |= UCSSEL_2;                  // SMCLK
      UCA0BR0 = 130;                         // 16MHz 9600
      UCA0BR1 = 6;                           // 16MHz 9600
      UCA0MCTL = UCBRS0;                     // Modulation UCBRSx = 1
      UCA0CTL1 &= ~UCSWRST;                  // Initialize USCI state machine
      
      __bis_SR_register(GIE);
   }
   
   void print_char(char data)				   
   {
      IE2 |= UCA0TXIE;							   // enable TX interrupt
      while (!(IFG2&UCA0TXIFG))              // USCI_A0 TX buffer ready?
         ;                   
		UCA0TXBUF =  data;
   }
   
   uint8_t read_char()
   {
      IE2 |= UCA0RXIE;							   // enable RX interrupt
      while (!(IFG2&UCA0RXIFG));             // USCI_A0 RX buffer ready?
      return UCA0RXBUF;
   }
   
   void print_string(char const *str)
   {
      while (*str)
		print_char(*str++);
   }
   
   void printline_string(char const *str)    // print string + new line
   {
      while(*str)
         print_char(*str++);
   
      print_char(0x0A);
   }

   void print_digit(uint32_t val)
   {
      char buff[16];
      std::ltoa(val, buff);
      printline_string(buff);
   }   
}

#pragma vector = USCIAB0RX_VECTOR;
__interrupt void USCIA0RX_ISR (void)
{
   IE2 &= ~UCA0RXIE;						         // disable interrupt of RX
}

#pragma vector = USCIAB0TX_VECTOR;
__interrupt void USCIA0TX_ISR (void)
{
   IE2 &= ~UCA0TXIE; 					         // disbale interrupt of TX
}
   
   
