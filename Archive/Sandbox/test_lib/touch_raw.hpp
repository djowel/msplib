/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#if !defined(TOUCH_HPP_SEPTEMBER_28_2012)
#define TOUCH_HPP_SEPTEMBER_28_2012

#include <msp430.h>
#include <stdint.h>
#include <algorithm>

namespace cycfi
{
   uint16_t timer_count;
   uint16_t capacitance_val;
   uint16_t base;
   uint16_t prev_val;
   uint16_t curr_val; 
   uint8_t out_bit = BIT4;
   uint8_t in_bit = BIT5;
   
   // template<int ID, int out_bit, int in_bit>
   struct touch_sense
   {

      touch_sense()
      {   
         
         for (int i = 0; i < 100; i ++)
         {                        
            curr_val = (prev_val + capacitance_val) /2;
            prev_val = curr_val;
         }
         
         base = curr_val;
      }
      
      uint16_t measure(uint8_t pad)
      {
         uint16_t current = sense_key(pad);
         return (current <= base) ? 0 : (current - base);
      }
      
      inline uint16_t sense_key(uint8_t pad)
      {
         P1OUT &= ~(out_bit + in_bit);
         P1OUT |= pad;
         P1IES |= pad;
         P1IE |= pad;
         P1DIR &= ~pad;
         timer_count = TAR;

         __bis_SR_register( LPM0_bits + GIE );
         P1IE &= ~pad;
         P1OUT &= ~pad;
         P1DIR |= pad;
         capacitance_val = timer_count;

         P1OUT |= (out_bit + in_bit)^pad;
         P1IES &= ~pad;
         P1IE |= pad;
         P1DIR &= ~pad;
         timer_count = TAR;

         __bis_SR_register( LPM0_bits + GIE );
         P1IE &= ~pad;
         P1OUT &= ~(out_bit + in_bit);
         P1DIR |= (out_bit + in_bit);

         return capacitance_val += timer_count;
      }   
   };        
}

#pragma vector = PORT1_VECTOR
__interrupt void port1_isr()
{
   P1IFG = 0;
   cycfi::timer_count = TAR - cycfi::timer_count;
   __bic_SR_register_on_exit( LPM0_bits );
}

#pragma vector = TIMER0_A1_VECTOR
__interrupt void TIMER0_A1_ISR()
{
	switch(TAIV)
	{
		case 2:
			break;
		case 4:
			break;
		case 10:
			TACTL &= ~TAIFG;
			break;

	}
}





#endif