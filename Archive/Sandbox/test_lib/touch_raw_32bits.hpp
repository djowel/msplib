/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#if !defined(TOUCH_HPP_SEPTEMBER_28_2012)
#define TOUCH_HPP_SEPTEMBER_28_2012

#include <msp430.h>
#include <stdint.h>
#include <algorithm>

namespace cycfi
{
   uint32_t timer_count;  
   uint8_t output_bit = BIT4;
   uint8_t input_bit = BIT5;
   uint32_t tar_counter;
   uint32_t sys_tick();
   
   // template<int ID, int out_bit, int in_bit>
   struct touch_sense
   {
      touch_sense()
      {          
         uint32_t prev_val;
         uint32_t curr_val;
         for (int i = 0; i < 100; i ++)
         {                        
            curr_val = (prev_val + capacitance_val) /2;
            prev_val = curr_val;
         }
         base = curr_val;         
      }
      
      uint32_t measure(uint8_t pad)
      {
         uint32_t current = sense_key(pad);
         return (current <= base) ? 0 : (current - base);      
      }
      
      inline uint32_t sense_key(uint8_t pad)
      {   
         P1OUT &= ~(output_bit + input_bit);
         P1OUT |= pad;
         P1IES |= pad;
         P1IE |= pad;
         P1DIR &= ~pad;
         timer_count = sys_tick();

         __bis_SR_register( LPM0_bits + GIE );
         P1IE &= ~pad;
         P1OUT &= ~pad;
         P1DIR |= pad;
         capacitance_val = timer_count;

         P1OUT |= (output_bit + input_bit)^pad;
         P1IES &= ~pad;
         P1IE |= pad;
         P1DIR &= ~pad;
         timer_count = sys_tick();
         
         __bis_SR_register( LPM0_bits + GIE );
         P1IE &= ~pad;
         P1OUT &= ~(output_bit + input_bit);
         P1DIR |= (output_bit + input_bit);

         return capacitance_val += timer_count;
      } 

      uint32_t base;
      uint32_t capacitance_val; 
      uint32_t tar_counter;
   }; 
   
   inline void tar_interrupt()
   {
      tar_counter++;
   } 

   uint32_t sys_tick()
   {
      return TAR | (uint32_t(tar_counter)<<16);
   } 
}

#pragma vector = PORT1_VECTOR
__interrupt void port1_isr()
{
   P1IFG = 0;
   cycfi::timer_count = cycfi::sys_tick() - cycfi::timer_count;
   __bic_SR_register_on_exit( LPM0_bits);
}

#pragma vector = TIMER0_A1_VECTOR
__interrupt void TIMER0_A1_ISR()
{
	switch(TAIV)
	{
		case 2:
			break;
		case 4:
			break;
		case 10:
         cycfi::tar_interrupt();
			TACTL &= ~TAIFG;
			break;

	}
}

#endif