#include <msp430g2553.h>

int main()
{
	  WDTCTL = WDTPW+WDTHOLD;                   // Stop WDT
	  BCSCTL1 = XT2OFF + RSEL3 + RSEL2 + RSEL1 + RSEL0; // 16MHz clock settings
	  DCOCTL = DCO2 + DCO0;

	  ADC10CTL0 = ADC10SHT_2 + ADC10ON + ADC10IE; // ADC10ON, interrupt enabled
	  ADC10CTL1 = INCH_1;                       // input A1
	  ADC10AE0 |= 0x02;                         // PA.1 ADC option select
	  P1DIR |= 0x01;                            // Set P1.0 to output direction

	  TA0CTL = TASSEL_2 | TACLR;
	  TA0CCTL1 = CM_0 | OUTMOD_0 | CCIE;
	  TA0CCTL0 = CCIE;

	  TA0CCR0 = 333;
	  TA0CCR1 = 0;

	  TA0CCTL1 &= ~(OUTMOD_7 | OUT);
	  P1DIR |= BIT3 + BIT4;
	  P1OUT |= ~BIT2;

	  TA0CTL |= MC_1;

	  TA0CTL &= ~MC_0;
	  TA0CCTL1 &= ~(OUTMOD_7 | OUT);

	  _BIS_SR(GIE);

	  while(1)
	  {
		  ADC10CTL0 |= ENC + ADC10SC;             // Sampling and conversion start
		  __bis_SR_register(CPUOFF + GIE);        // LPM0, ADC10_ISR will force exit
	  }

}

#pragma vector = TIMER0_A0_VECTOR
__interrupt void timer0_A0_isr()
{
	P1OUT |= BIT4;
	int count = 0;
	count +=1;

//	if(count == 4)
//	{
		TA0CCR1 = (ADC10MEM >> 2) + 35;
//		count = 0;
//	}
}

#pragma vector = TIMER0_A1_VECTOR
__interrupt void timer0_A1_isr()
{
	switch(TA0IV)
	{
		case 2:
		P1OUT &= ~BIT4;
			break;
		case 4:
			break;
		case 10:
			break;
		default:
			break;
	}
}

// ADC10 interrupt service routine
#pragma vector=ADC10_VECTOR
__interrupt void ADC10_ISR(void)
{
  __bic_SR_register_on_exit(CPUOFF);        // Clear CPUOFF bit from 0(SR)
}
