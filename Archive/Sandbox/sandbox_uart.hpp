/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#if !defined(SANDBOX_UART_HPP_OCTOBER_8_2012)
#define SANDBOX_UART_HPP_OCTOBER_8_2012

#include <msp430.h>
#include <stdint.h>
#include <cstdlib>

namespace cycfi
{
   void uart_setup(uint32_t baud)
   {
      uint32_t bits = cycfi::clock_speed/baud;
      P1SEL  = BIT1 + BIT2;
      P1SEL2 = BIT1 + BIT2;
      UCA0CTL1 |= UCSSEL_2;                  // SMCLK
      UCA0BR0 = bits & 0xFF;                 // 16MHz 9600
      UCA0BR1 = bits >>8;                    // 16MHz 9600
      UCA0MCTL = UCBRS0;                     // Modulation UCBRSx = 1
      UCA0CTL1 &= ~UCSWRST;                  // Initialize USCI state machine
      
      __bis_SR_register(GIE);
   }
   
   inline void write(uint8_t data)
   {
      IE2 |= UCA0TXIE;                        // enable TX interrupt
      while (!(IFG2&UCA0TXIFG))               // USCI_A0 TX buffer ready?
    	  _bis_SR_register(LPM0_bits + GIE);
      UCA0TXBUF =  data;
   }
   
   inline void write_string(char const *str)        // print string + new line
   {
      while(*str)
         write(*str++);
   
      write(0x0A);
   }
   
   inline uint8_t read()
   {
      IE2 |= UCA0RXIE;							   // enable RX interrupt
      while (!(IFG2&UCA0RXIFG))              // USCI_A0 RX buffer ready?
         _bis_SR_register(LPM0_bits + GIE);
      return UCA0RXBUF;
   }
   
   inline void print_digit(uint32_t val)
   {
      char buff[16];
      std::ltoa(val, buff);
      write_string(buff);
   } 

   void print_values(int16_t a, int16_t b, int16_t c, int16_t d)
   {
      write(0xAB);         // magic lo
      write(0xCD);         // magic hi
      write(8);            // payload lo
      write(0);            // payload hi
      write(a & 0xFF);     // lsb
      write(a >> 8);       // msb
      write(b & 0xFF);     // lsb
      write(b >> 8);       // msb
      write(c & 0xFF);     // lsb
      write(c >> 8);       // msb
      write(d & 0xFF);     // lsb
      write(d >> 8);       // msb
   }
}

#endif

#pragma vector = USCIAB0RX_VECTOR;
__interrupt void USCIA0RX_ISR (void)
{
   IE2 &= ~UCA0RXIE;						         // disable interrupt of RX
   _bic_SR_register_on_exit(LPM0_bits);
}

#pragma vector = USCIAB0TX_VECTOR;
__interrupt void USCIA0TX_ISR (void)
{
   IE2 &= ~UCA0TXIE; 					         // disbale interrupt of TX
   _bic_SR_register_on_exit(LPM0_bits);
}