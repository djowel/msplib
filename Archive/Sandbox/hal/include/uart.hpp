/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#if !defined(UART_H_ASEPTEMBER_10_2012)
#define UART_H_ASEPTEMBER_10_2012

namespace cycfi { namespace lib
{
	class uart
	{
	
		public:
			uart();
		private:

			void ConfigWDT();
			void ConfigClocks();
			void ConfigPins();
			void ConfigTimerA2();



         
	};
}}

#endif
