/************************************************************
* STANDARD BITS
************************************************************/

#define BIT0                   (0x0001)
#define BIT1                   (0x0002)
#define BIT2                   (0x0004)
#define BIT3                   (0x0008)
#define BIT4                   (0x0010)
#define BIT5                   (0x0020)
#define BIT6                   (0x0040)
#define BIT7                   (0x0080)
#define BIT8                   (0x0100)
#define BIT9                   (0x0200)
#define BITA                   (0x0400)
#define BITB                   (0x0800)
#define BITC                   (0x1000)
#define BITD                   (0x2000)
#define BITE                   (0x4000)
#define BITF                   (0x8000)

/*----------------------------------------------------------------------------*/
/* PERIPHERAL FILE MAP                                                        */
/*----------------------------------------------------------------------------*/

/* External references resolved by a device-specific linker command file */
#define SFR_8BIT(address)   extern volatile unsigned char address
#define SFR_16BIT(address)  extern volatile unsigned int address

/************************************************************
* WATCHDOG TIMER
************************************************************/
SFR_16BIT(WDTCTL);

#define WDTPW                  (0x5A00)
#define WDTSSEL                (0x0004)
#define WDTCNTCL               (0x0008)
#define WDTIS0                 (0x0001)
#define WDTTMSEL               (0x0010)
#define WDTHOLD                (0x0080)
#define WDT_ADLY_250           (WDTPW+WDTTMSEL+WDTCNTCL+WDTSSEL+WDTIS0)          /* 250ms   " */

/************************************************************
* SPECIAL FUNCTION REGISTER ADDRESSES + CONTROL BITS
************************************************************/
SFR_8BIT(IE1);                                /* Interrupt Enable 1 */
#define WDTIE                  (0x01)         /* Watchdog Interrupt Enable */
SFR_8BIT(IFG1);                               /* Interrupt Flag 1 */
#define OFIFG                  (0x02)         /* Osc. Fault Interrupt Flag */

/************************************************************
* Basic Clock Module
************************************************************/
SFR_8BIT(DCOCTL);                             /* DCO Clock Frequency Control */
SFR_8BIT(BCSCTL1);                            /* Basic Clock System Control 1 */
SFR_8BIT(BCSCTL2);                            /* Basic Clock System Control 2 */
SFR_8BIT(BCSCTL3);                            /* Basic Clock System Control 3 */

#define LFXT1S_2               (0x20)         /* Mode 2 for LFXT1 : VLO */

/************************************************************
* Calibration Data in Info Mem
************************************************************/

#ifndef __DisableCalData

SFR_8BIT(CALDCO_1MHZ);                        /* DCOCTL  Calibration Data for 1MHz */
SFR_8BIT(CALBC1_1MHZ);                        /* BCSCTL1 Calibration Data for 1MHz */

#endif /* #ifndef __DisableCalData */

/************************************************************
* DIGITAL I/O Port1/2 Pull up / Pull down Resistors
************************************************************/
SFR_8BIT(P1SEL);                              /* Port 1 Selection */
SFR_8BIT(P1DIR);                              /* Port 1 Direction */
SFR_8BIT(P1OUT);                              /* Port 1 Output */

/************************************************************
* Timer A2
************************************************************/
SFR_16BIT(TACCTL0);                           /* Timer A Capture/Compare Control 0 */
#define CCTL0                  TACCTL0        /* Timer A Capture/Compare Control 0 */
#define OUT                    (0x0004)       /* PWM Output signal if output mode 0 */

SFR_16BIT(TACTL);                             /* Timer A Control */
#define TASSEL_2               (2*0x100u)     /* Timer A clock source select: 2 - SMCLK */
#define MC_2                   (2*0x10u)      /* Timer A mode control: 2 - Continous up */
#define ID_3                   (3*0x40u)      /* Timer A input divider: 3 - /8 */

SFR_16BIT(TAR);
SFR_16BIT(TACCR0);                            /* Timer A Capture/Compare 0 */
#define CCR0                   TACCR0         /* Timer A Capture/Compare 0 */

#define CCIS0                  (0x1000)       /* Capture input select 0 */
#define OUTMOD0                (0x0020)       /* Output mode 0 */
#define CCIE                   (0x0010)       /* Capture/compare interrupt enable */

#define OUTMOD2                (0x0080)       /* Output mode 2 */

/************************************************************
* Interrupt Vectors (offset from 0xFFE0)
************************************************************/
#ifdef __ASM_HEADER__ /* Begin #defines for assembler */
#define TIMERA0_VECTOR          ".int09"                    /* 0xFFF2 Timer A CC0 */
#else
#define TIMERA0_VECTOR          (9 * 1u)                     /* 0xFFF2 Timer A CC0 */
/*#define TIMERA0_ISR(func)       ISR_VECTOR(func, ".int09")  */ /* 0xFFF2 Timer A CC0 */ /* CCE V2 Style */
#endif

#ifdef __ASM_HEADER__ /* Begin #defines for assembler */
#define WDT_VECTOR              ".int10"                    /* 0xFFF4 Watchdog Timer */
#else
#define WDT_VECTOR              (10 * 1u)                    /* 0xFFF4 Watchdog Timer */
/*#define WDT_ISR(func)           ISR_VECTOR(func, ".int10")  */ /* 0xFFF4 Watchdog Timer */ /* CCE V2 Style */
#endif

/************************************************************
* LOW POWER MODES
************************************************************/
#define LPM3_bits              (SCG1+SCG0+CPUOFF)

/************************************************************
* STATUS REGISTER BITS
************************************************************/
#define SCG1                   (0x0080)
#define SCG0                   (0x0040)
#define CPUOFF                 (0x0010)
