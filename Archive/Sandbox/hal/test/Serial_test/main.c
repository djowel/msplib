//***********************************************************
// Lab7.c  Software UART
//
// SFB 1/2012
//***********************************************************

#include <msp430g2231.h>
#include "../../include/uart.hpp"

#ifndef TIMER0_A1_VECTOR
#define TIMER0_A1_VECTOR    TIMERA1_VECTOR
#define TIMER0_A0_VECTOR    TIMERA0_VECTOR
#endif



#define TXD BIT1							// TXD on P1.1
#define RXD	BIT2     						// RXD on P1.2
#define Bitime 13*4							// 0x0D

unsigned char TXByte;
unsigned char BitCnt;
volatile int i;
unsigned int TxLO[]={0x4C,0x4F,0x0A,0x08,0x08};

void ConfigWDT(void);
void ConfigClocks(void);
void ConfigPins(void);
void ConfigTimerA2(void);
void write(unsigned char data);

void main(void)
{
  ConfigWDT();
  ConfigClocks();
  ConfigPins();
  ConfigTimerA2();

  while(1)
  {
   _bis_SR_register(LPM3_bits + GIE);		// turn on interrupts and LPM3
   write('z');
  }
}

void ConfigWDT(void)
{
  WDTCTL = WDT_ADLY_250;      				// <1 sec WDT interval
  IE1 |= WDTIE;               				// Enable WDT interrupt
}

void ConfigClocks(void)
{
  BCSCTL1 = CALBC1_1MHZ; 					// Set range
  DCOCTL = CALDCO_1MHZ;  					// Set DCO step + modulation
  BCSCTL3 |= LFXT1S_2;                      // LFXT1 = VLO
  IFG1 &= ~OFIFG;                           // Clear OSCFault flag
  BCSCTL2 = 0;               				// MCLK = DCO = SMCLK
}

void ConfigPins(void)
 {
  P1SEL |= TXD + RXD;						// P1.1 & 2 TA0, rest GPIO
  P1DIR = ~(BIT3 + RXD);					// P1.3 input, other outputs
  P1OUT = 0;              					// clear output pins
 }

void ConfigTimerA2(void)
{
   CCTL0 = OUT;                             // TXD Idle as Mark
   TACTL = TASSEL_2 + MC_2 + ID_3;          // SMCLK/8, continuous mode

}

// Function writes Character from TXByte
void write(unsigned char data)
{
  TXByte = data;
  BitCnt = 0xA;                     		// Load Bit counter, 8data + ST/SP
  while (CCR0 != TAR)               		// Prevent async capture
    CCR0 = TAR;                     		// Current state of TA counter
  CCR0 += Bitime;                   		// Some time till first bit
  TXByte |= 0x100;                  		// Add mark stop bit to TXByte
  TXByte = TXByte << 1;             		// Add space start bit
  CCTL0 =  CCIS0 + OUTMOD0 + CCIE;  		// TXD = mark = idle
  while ( CCTL0 & CCIE );           		// Wait for TX completion
}

// Timer A0 interrupt service routine
#pragma vector=TIMER0_A0_VECTOR
__interrupt void Timer_A (void)
{
  CCR0 += Bitime;                   		// Add Offset to CCR0
  if (CCTL0 & CCIS0)                		// TX on CCI0B?
  {
    if ( BitCnt == 0)
    {
      CCTL0 &= ~ CCIE ;             		// All bits TXed, disable interrupt
    }

    else
    {
      CCTL0 |=  OUTMOD2;            // TX Space
      if (TXByte & 0x01)
      CCTL0 &= ~ OUTMOD2;           // TX Mark
      TXByte = TXByte >> 1;
      BitCnt --;
    }
  }
}

// WDT interrupt service routine
#pragma vector=WDT_VECTOR
__interrupt void WDT(void)
{
  _bic_SR_register_on_exit(LPM3_bits);
}
