/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/

#include "../include/uart.hpp"
#include "../common/msp430g2553.h"

namespace cycfi {namespace lib
{
	uart::uart()
	{
		ConfigWDT();
		ConfigClocks();
		ConfigPins();
		ConfigTimerA2();
	}


	void uart::ConfigWDT()
	{
		WDTCTL = WDT_ADLY_250;      				// <1 sec WDT interval
		IE1 |= WDTIE;          				// Enable WDT interrupt
	}

	void uart::ConfigClocks()
	{
	  BCSCTL1 = CALBC1_1MHZ; 					// Set range
	  DCOCTL = CALDCO_1MHZ;  					// Set DCO step + modulation
	  BCSCTL3 |= LFXT1S_2;                      // LFXT1 = VLO
	  IFG1 &= ~OFIFG;                           // Clear OSCFault flag
	  BCSCTL2 = 0;               				// MCLK = DCO = SMCLK
	}

	void uart::ConfigPins()
	{
	  P1SEL |= BIT1 + BIT2;						// P1.1 & 2 TA0, rest GPIO
	  P1DIR = ~(BIT3 + BIT2);					// P1.3 input, other outputs
	  P1OUT = 0;              					// clear output pins
	}

	void uart::ConfigTimerA2()
	{
//	   CCTL0 = OUT;                             // TXD Idle as Mark
//	   TACTL = TASSEL_2 + MC_2 + ID_3;          // SMCLK/8, continuous mode

	}




}}
