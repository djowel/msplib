#include <cycfi.hpp>


#define RED BIT0
#define GRN BIT6

#define DOT BIT4
#define DASH BIT5

uint32_t tar_hi = 0;
uint32_t systick();

volatile uint16_t timer_count;

inline void enter_sleep_mode()
{
    __bis_SR_register( LPM0_bits + GIE );
}

inline void capture_timer_count()
{
   timer_count = systick();
}

inline void set_keys_to_low()
{
   P1OUT &= ~(BIT4 + BIT5);
}

inline void charge_key(uint16_t key)
{
   P1OUT |= key;
}

inline void interrupt_triggered_on_rising_edge(uint16_t key)
{
   P1IES |= key;                              // Trigger on voltage drop.
   P1IE |= key;                               // Interrupt on.
   P1DIR &= ~key;                             // Float key and let voltage drop.
}

inline void discharge_key(uint16_t key)
{
   P1IE &= ~key;                              // Disable interrupts on key.
   P1OUT &= ~key;                             // Discharge key by setting
   P1DIR |= key;                              // active low.
}

inline void charge_complement_line(uint16_t key)
{
   P1OUT |= (BIT4 + BIT5)^key;
}

inline void interrupt_triggered_on_falling_edge(uint16_t key)
{
   P1IES &= ~key;                             // Trigger on voltage rise.
   P1IE |= key;                               // Interrupt on.
   P1DIR &= ~key;                             // Float key and let voltage rise.
}

inline void discharge_complement_line(uint16_t key)
{
   P1IE &= ~key;                              // Disable interrupts on key.
   P1OUT &= ~(BIT4 + BIT5);                   // Set both keys to
   P1DIR |=  (BIT4 + BIT5);                   // active low.
}

inline void initial_setup()
{
   WDTCTL = WDTPW + WDTHOLD;
   BCSCTL1 = CALBC1_16MHZ;
   DCOCTL = CALDCO_16MHZ;
   TACTL = MC_2 + TASSEL_2 + TAIE;
}

inline void pin_setup()
{
   P1DIR |= DASH + DOT;
}

inline void watch_dog_timer_interval_at_0_5_ms()
{
   WDTCTL = WDT_MDLY_0_064;
   IE1 |= WDTIE;
}

uint16_t measure_key_capacitance( uint16_t key )
{
   set_keys_to_low();
   charge_key(key);
   interrupt_triggered_on_rising_edge(key);
   capture_timer_count();
   enter_sleep_mode();
   discharge_key(key);

   static uint16_t sum                        // Save the count that was
      = timer_count;                          // recorded in interrupt.

   charge_complement_line(key);
   interrupt_triggered_on_falling_edge(key);
   capture_timer_count();
   enter_sleep_mode();
   discharge_complement_line(key);

   return sum + timer_count;                  // Return the sum of both counts.
}

#define baud_rate 1666
unsigned char TXByte;
unsigned char BitCnt;


void ConfigPins()
{
	P1SEL |= BIT1;
	P1DIR |= BIT1;
	P1OUT = 0;
}

// Function writes Character from TXByte
void write(char data)
{
	while ( CCTL0 & CCIE );
	TXByte = data;
	BitCnt = 10;
	CCR0 += baud_rate;
	TXByte |= 0x100;
	TXByte = TXByte << 1;
	CCTL0 =  CCIS0 + OUTMOD_1 + CCIE;
}

void write_string(char const* str)
{
	while(*str)
		write(*str++);
}

void writeline_string(char const* str)
{
	while(*str)
		write(*str++);

	write('\n');
}

void writeline_digit(long val)
{
	char buff[16];
	std::ltoa(val, buff);
	write_string(buff);
}

void tar_interrupt()
{
	tar_hi++;
}

uint32_t systick()
{
	return TAR| (uint32_t(tar_hi)<<16);
}

//*********************************************************************//
int main()
{
   initial_setup();
   pin_setup();
   ConfigPins();
   CCTL0 = OUTMOD_0 + OUT;

   _EINT();

   while (1)
   {
	    writeline_digit(measure_key_capacitance(DOT)/128);
	   	write ('\n');
	    cycfi::delay_ms(100);
   }
}

//*********************************************************************//

#pragma vector = PORT1_VECTOR
__interrupt void port1_isr()
{
   P1IFG = 0;
   timer_count = systick() - timer_count;
   __bic_SR_register_on_exit( LPM0_bits );
}

#pragma vector=TIMER0_A0_VECTOR
__interrupt void Timer_A ()
{
	CCR0 += baud_rate;
	if (CCTL0 & CCIS0)
	{
		if ( BitCnt == 0)
			CCTL0 &= ~ CCIE ;
		else
		{
			CCTL0 |=  OUTMOD_4;
			if (TXByte & 0x01)
				CCTL0 &= ~ OUTMOD_4;
			TXByte = TXByte >> 1;
			BitCnt --;
		}
	}
}

#pragma vector = TIMER0_A1_VECTOR
__interrupt void TIMER0_A1_ISR()
{
	switch(TAIV)
	{
		case 2:
			break;
		case 4:
			break;
		case 10:
			tar_interrupt();
			TACTL &= ~TAIFG;
			break;

	}
}


