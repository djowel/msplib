#include <msp430g2231.h>

unsigned int wdtCounter = 0;

#ifndef TIMER0_A1_VECTOR
#define TIMER0_A1_VECTOR    TIMERA1_VECTOR
#define TIMER0_A0_VECTOR    TIMERA0_VECTOR
#endif
#define baud_rate 13
unsigned char TXByte;
unsigned char BitCnt;

unsigned int timerCount = 0;

void main(void)
{
	WDTCTL = WDT_MDLY_32; // Set Watchdog Timer interval to ~32ms
	IE1 |= WDTIE; // Enable WDT interrupt
	P1DIR |= BIT0; // Set P1.0 to output direction
	P1OUT |= BIT0; // Turn on LED at 1.0
	P1IE |= BIT3; // enable P1.3 interrupt

	CCTL0 = OUT;
	TACTL = TASSEL_2 + MC_2 + ID_3;

	__enable_interrupt();

	for(;;)
	{

	}
}

// Watchdog Timer interrupt service routine
#pragma vector=WDT_VECTOR
__interrupt void watchdog_timer(void)
{
	if(wdtCounter == 249)
	{
		P1OUT = 0x00; // P1.0 turn off
		wdtCounter = 0;
		_BIS_SR(LPM3_bits + GIE); // Enter LPM3 w/interrupt
	}
	else
	{
		wdtCounter++;
	}

}
// Port 1 interrupt service routine


#pragma vector=TIMERA0_VECTOR
__interrupt void Timer_A (void)
{
	P1OUT |= 0x01; // turn LED on
	_BIC_SR(LPM3_EXIT); // wake up from low power mode
}


