#include <msp430g2231.h>

unsigned int counter = 0;
void main(void)
{
	WDTCTL = WDTPW + WDTHOLD;
	P1DIR |= 0x01;
	P1OUT &= ~0x01;
	CCTL0 = CCIE;
	CCR0 = 60000;
	TACTL = TASSEL_2 + MC_2;


	_BIS_SR(LPM0_bits + GIE);
}

#pragma vector=TIMERA0_VECTOR
__interrupt void Timer_A (void)
{
	CCR0 += 60000;
	counter +=1;
	if(counter == 50)
	{
		P1OUT |= 0x01;
		counter = 0;
	}
}


//////////////////////////////////////////////////////////////////////////////

// config.hpp

namespace cycfi
{
   int const clock_speed = 1000000;
}

// main.hpp

namespace cycfi
{
   void setup();
   void loop();
}

// main.cpp

unsigned short interrupt_period = 60000;


void setup_xxx()
{
}

void main()
{
   // put these into a separate setup function
	WDTCTL = WDTPW + WDTHOLD;
	CCTL0 = CCIE;
	CCR0 = interrupt_period;
	TACTL = TASSEL_2 + MC_2;

   cycfi::setup();

	while (true)
      cycfi::loop();
}

#pragma vector=TIMERA0_VECTOR
__interrupt void Timer_A()
{
	CCR0 += interrupt_period;
}

// core.hpp

namespace cycfi
{
   void delay_ms(int ms);
}

// core.cpp

namespace cycfi
{
   void delay_ms(int ms)
   {
      // implement me...
   }
}

// blink.cpp
namespace cycfi
{
   void setup()
   {
   }

   void loop()
   {
   }
}


