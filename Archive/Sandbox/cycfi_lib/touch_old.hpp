/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#include <cycfi.hpp>

namespace cycfi
{
   template<>
   struct task<2> : task_base<2>
   {
      static uint16_t const period = 160000;
      
      task()
      {
         P1DIR |= BIT4 + BIT5;
      }
      
      void perform()
      {
         tar_overflow_count++;
      }
      
      uint32_t tar_overflow_count;
      uint32_t timer_count;
      uint32_t capacitance_value;
   };
   
   inline uint32_t systick()
   {
      extern cycfi::task<2>* task2; 
      return TAR | (uint32_t(task2->tar_overflow_count)<<16);
   }
   
   inline uint16_t measure_key(uint8_t PAD1, uint8_t PAD2, uint8_t pad)
   {
      extern cycfi::task<2>* task2; 
      P1OUT &= ~(PAD1 + PAD2);
      P1OUT |= pad;
      P1IES |= pad;
      P1IE |= pad;
      P1DIR &= ~pad; 
      task2->timer_count = systick();

      LPM0;
      P1IE &= ~pad;
      P1OUT &= ~pad;
      P1DIR |= pad;
      task2->capacitance_value = task2->timer_count;
      
      P1OUT &= ~(PAD1 + PAD2)^pad;
      P1IES &= ~pad;
      P1IE |= pad;
      P1DIR &= ~pad;
      task2->timer_count = systick();
      
      LPM0;
      P1IE &= ~pad;
      P1OUT &= ~(PAD2 + PAD2);
      P1DIR |= (PAD2 + PAD2);
      
      return task2->capacitance_value = task2->capacitance_value + task2->timer_count;
   }
}