/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#if !defined(CONFIG_H_SEPTEMBER_14_2012)
#define CONFIG_H_SEPTEMBER_14_2012

namespace cycfi
{
   unsigned long const clock_speed = 1000000;
}

#endif

