/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#if !defined(MAIN_H_SEPTEMBER_14_2012)
#define MAIN_H_SEPTEMBER_14_2012

namespace cycfi
{
   void setup();
   void loop();
}
#endif
