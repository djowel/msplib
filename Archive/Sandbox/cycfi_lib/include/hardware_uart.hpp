/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#if !defined(HARDWARE_UART_HPP_SEPTEMBER_22_2012)
#define HARDWARE_UART_HPP_SEPTEMBER_22_2012

#include <msp430.h>
#include <stdint.h>

namespace cycfi
{
   void hardware_uart_setup();
   void print_char(char data);
   uint8_t read_char();
   void print_string(char const *str);
   void printline_string(char const *str);
   void print_number(uint32_t value);
}

#endif
