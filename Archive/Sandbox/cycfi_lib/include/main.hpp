/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#if !defined(MAIN_HPP_SEPTEMBER_14_2012)
#define MAIN_HPP_SEPTEMBER_14_2012

namespace cycfi
{
   void setup();
   void loop();
}

#endif
