/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#if !defined(CONFIG_HPP_SEPTEMBER_14_2012)
#define CONFIG_HPP_SEPTEMBER_14_2012

namespace cycfi
{
   unsigned long const clock_speed = 1000000;
   unsigned short const interrupt_period = 60000;
}

#endif

