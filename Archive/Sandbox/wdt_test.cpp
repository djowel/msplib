#include<msp430g2553.h>
#include <stdint.h>

namespace cycfi
{
	uint32_t const clk = 16000000;
	uint32_t const div = 1000;
}

class delay
{
public:
	delay():time(0){}

	void ms(uint32_t ms)
	{
		_bis_SR_register(LPM0_bits);

		while(--ms)
			_delay_cycles(cycfi::clk/cycfi::div);

		time = ms;
	}

	 uint16_t get_time(){return time;}

private:

	 uint32_t time;
};

int main()
{
	  WDTCTL = WDTPW + WDTHOLD;
	  BCSCTL1 = CALBC1_16MHZ;
	  DCOCTL = CALDCO_16MHZ;

	  WDTCTL = WDT_MDLY_0_5;
	  IE1|= WDTIE;
	  P1DIR=BIT0;
	  P1OUT=BIT0;
	  _EINT();

	  while(1)
	  {
		  delay timer;

		  P1OUT |= 0x01;
		  timer.ms(100);
		  P1OUT &= ~0x01;
		  timer.ms(100);
	  }
}

#pragma vector = WDT_VECTOR
__interrupt void wdt_isr()
{
	delay timer;
	if(timer.get_time()==0)
		_bic_SR_register_on_exit(LPM0_bits);

}





