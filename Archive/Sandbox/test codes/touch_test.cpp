/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#include <touch.hpp>

#define baud_rate 1666
uint8_t TXByte;
uint8_t BitCnt;

void ConfigPins()
{
	P1SEL |= BIT1;
	P1DIR |= BIT1;
	P1OUT = 0;
}

// Function writes Character from TXByte
void write(char data)
{
	while ( CCTL0 & CCIE );
	TXByte = data;
	BitCnt = 10;
	CCR0 += baud_rate;
	TXByte |= 0x100;
	TXByte = TXByte << 1;
	CCTL0 =  CCIS0 + OUTMOD_1 + CCIE;
}

void write_string(char const* str)
{
	while(*str)
		write(*str++);
}

void writeline_string(char const* str)
{
	while(*str)
		write(*str++);

	write('\n');
}

void writeline_digit(long val)
{
	char buff[16];
	std::ltoa(val, buff);
	writeline_string(buff);
}

int main()
{
	WDTCTL = WDTPW + WDTHOLD;
	BCSCTL1 = CALBC1_16MHZ;
	DCOCTL = CALDCO_16MHZ;
	TACTL = TASSEL_2 + MC_2 + TAIE;
	CCTL0 = OUTMOD_0 + OUT;
	ConfigPins();

	_EINT();

	while (1)
	{
		cycfi::touch_sense t;
		writeline_digit(t.measure(BIT5));
		_delay_cycles(1000000);
	}
}

#pragma vector=TIMER0_A0_VECTOR
__interrupt void Timer0_A0_ISR ()
{
	CCR0 += baud_rate;
	if (CCTL0 & CCIS0)
	{
		if ( BitCnt == 0)
			CCTL0 &= ~ CCIE ;
		else
		{
			CCTL0 |=  OUTMOD_4;
			if (TXByte & 0x01)
				CCTL0 &= ~ OUTMOD_4;
			TXByte = TXByte >> 1;
			BitCnt --;
		}
	}
}




