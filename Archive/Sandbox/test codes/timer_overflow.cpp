#include <msp430g2553.h>
#include<stdint.h>
uint32_t tar_hi;
int main()
{
	WDTCTL = WDTPW + WDTHOLD;
	BCSCTL1 = CALBC1_1MHZ;
	DCOCTL = CALDCO_1MHZ;
	TACTL = TASSEL_2 + MC_2 + TAIE;
	P1DIR |= 0x41;
	CCTL1 = CCIE;
	CCR1 = 0;

	_EINT();
}



#pragma vector = TIMER0_A1_VECTOR
__interrupt void TIMER0_A1_ISR()
{
	switch(TAIV)
	{
		case 2:
			P1OUT |= 0x40;
			tar_hi++;
			CCR1 += 100000;
			break;

		case 4: break;

		case 10:
			P1OUT &= ~0x40;
			TACTL &= ~TAIFG;
			break;
	}
}
