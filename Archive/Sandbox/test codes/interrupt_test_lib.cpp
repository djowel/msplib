#include <cycfi.hpp>
#include <cycfi/debug.hpp>

uint16_t bit_mask;

namespace cycfi
{
   class my_interrupt1 : public pin_interrupt<1>
   {
   public:

      my_interrupt1()
         : base_type(4), led(out){}

      virtual void perform()
      {
    	 led = !led;
    	 delay_ms(500);
      }

   private:

      port_a0 led;
   };


   class my_interrupt2 : public pin_interrupt<1>
   {
   public:

      my_interrupt2()
         : base_type(5), led(out){}

      virtual void perform()
      {
    	 led = !led;
    	 delay_ms(500);
      }

   private:

      port_a6 led;
   };

   my_interrupt1 myint1;
   my_interrupt2 myint2;

   void main()
   {
//      myint1.enable(rising);
//      myint2.enable(rising);
      debug_out('a');
   }
}

#include <cycfi_main.ipp>

#pragma vector = PORT1_VECTOR
__interrupt void port1_ISR()
{
   bit_mask = P1IFG & 0xff;
   P1IFG = 0;
   cycfi::pin_interrupt<1>::call(bit_mask);
}
