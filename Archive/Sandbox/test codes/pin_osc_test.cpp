#include  "msp430g2553.h"
#include <uart.hpp>
#include <cycfi/touch.hpp>

int main()
{
  WDTCTL = WDTPW + WDTHOLD;                 // Stop watchdog timer
  BCSCTL1 = CALBC1_1MHZ;                    // Set DCO to 1MHz
  DCOCTL =  CALDCO_1MHZ;
  BCSCTL3 |= LFXT1S_2;                      // LFXT1 = VLO

  IE1 |= WDTIE;                             // enable WDT interrupt
  P2SEL = 0x00;                             // No XTAL

  cycfi::uart_setup(9600);

   _EINT();

  cycfi::touch_sense<2> p1(4);
  while (1)
  {
	  cycfi::print_values(p1.measure(),0,0,0);
  }
}

#pragma vector=WDT_VECTOR
__interrupt void watchdog_timer(void)
{
  TA0CCTL1 ^= CCIS0;                        // Create SW capture of CCR1
  __bic_SR_register_on_exit(LPM3_bits);     // Exit LPM3 on reti
}
