#include <cycfi.hpp>
#include <uart.hpp>
uint16_t data;

namespace cycfi
{

	void main_loop()
	{
		cycfi::hardware_uart_setup();

		P1IE |= 0x10 + 0x08;
		P1IES |=  0x10 + 0x08;
		P1IFG &= ~0x10 + 0x08;
	}
}


#include <cycfi_main.ipp>

#pragma vector = PORT1_VECTOR
__interrupt void port1_ISR()
{
	data = P1IFG & 0xff;
	P1IFG = 0;
	cycfi::print_digit(data);
}
