/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#include <cycfi.hpp>
#include <cycfi/debug.hpp>

namespace cycfi
{
   template <>
   struct task<2> : task_base<2>
   {
      static uint16_t const period = 16000;

      task() : led(out), i(0)
      {
         led = 1;
      }

      void perform()
      {
         if (i++ == 1000)
         {
            led = !led;
            i = 0;
         }
      }

      port_a6 led;  // port 1.6
      int i;
   };
  
   void main_loop()
   {
	   while (1)
	   {
         debug_out("Hello world");
         debug_out('\n');
         delay_ms(100);
	   }
   }
}

#include <cycfi_main.ipp>
