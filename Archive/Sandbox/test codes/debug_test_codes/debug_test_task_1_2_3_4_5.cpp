/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#include <cycfi.hpp>
#include <cycfi/debug.hpp>

namespace cycfi
{
   template <>
   struct task<2> : task_base<2>
   {
      static uint16_t const period = 16000;

      task() : led(out), i(0)
      {
         led = 1;
      }

      void perform()
      {
         if (i++ == 500)
         {
            led = !led;
            i = 0;
         }
      }

      port_a0 led;  // port 1.0
      int i;
   };

   template <>
   struct task<3> : task_base<3>
   {
	  static uint16_t const period = 16000;

	  task() : led(out), i(0)
      {
		  led = 1;
      }

      void perform()
      {
         if (i++ == 1000)
         {
            led = !led;
            i = 0;
         }
      }

      port_a2 led;  // port 1.2
      int i;
   };

   template <>
   struct task<4> : task_base<4>
   {
   	static uint16_t const period = 16000;

   	task() : led(out), i(0)
      {
   		led = 1;
      }

      void perform()
      {
       	if (i++ == 100)
         {
       		led = !led;
            i = 0;
         }
      }

      port_a3 led;  // port 1.3
      int i;
   };

   template <>
   struct task<5> : task_base<5>
   {
      static uint16_t const period = 16000;

   	task() : led(out), i(0)
      {
   		led = 1;
      }

      void perform()
      {
       	if (i++ == 2000)
         {
       		led = !led;
            i = 0;
         }
      }

      port_a4 led;  // port 1.4
      int i;
   };

   template <>
   struct task<6> : task_base<6>
   {
      static uint16_t const period = 16000;

      task() : led(out), i(0)
      {
      	led = 1;
      }

      void perform()
      {
         if (i++ == 4000)
         {
            led = !led;
            i = 0;
         }
      }

      port_a6 led;  // port 1.6
      int i;
    };

   void main_loop()
   {
	   while (1)
	   {
         debug_out("Hello world!");
         debug_out('\n');
         delay_ms(100);
	   }
   }
}

#include <cycfi_main.ipp>
