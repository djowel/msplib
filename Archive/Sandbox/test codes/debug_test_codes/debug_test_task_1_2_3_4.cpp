/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#include <cycfi.hpp>
#include <cycfi/debug.hpp>

namespace cycfi
{
   template <>
   struct task<2> : task_base<2>
   {
      static uint16_t const period = 16000;

      task() : led(out), i(0)
      {
         led = 1;
      }

      void perform()
      {
         if (i++ == 500)
         {
            led = !led;
            i = 0;
         }
      }

      port_a0 led;  // port 1.0
      int i;
   };

   template <>
   struct task<3> : task_base<3>
   {
      static uint16_t const period = 16000;

      task() : led(out), i(0)
      {
		  led = 1;
      }

      void perform()
      {
         if (i++ == 1000)
         {
            led = !led;
            i = 0;
         }
      }

      port_a2 led;  // port 1.2
      int i;
   };

   template <>
   struct task<4> : task_base<4>
   {
      static uint16_t const period = 16000;

      task() : led(out), i(0)
      {
         led = 1;
      }

      void perform()
      {
         if (i++ == 100)
         {
            led = !led;
            i = 0;
         }
      }

      port_a3 led;  // port 1.3
      int i;
   };


   void main_loop()
   {
	   while (1)
	   {
         debug_out("Hello world!");
         debug_out('\n');
         delay_ms(100);
	   }
   }
}

#include <cycfi_main.ipp>
