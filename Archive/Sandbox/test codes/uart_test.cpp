/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#include <cycfi.hpp>
#include <uart.hpp>

namespace cycfi
{
   void main()
   {
      uart_setup(9600);

      while (1)
      {
         if(read() == '1')
         {   
            write_string("Hello CYCFI");
            print_digit(2012);
         }
         delay_ms(3);
      }
   }
}

#include <cycfi_main.ipp>


