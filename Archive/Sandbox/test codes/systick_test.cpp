/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#include <cycfi.hpp>
#include <cycfi/debug.hpp>
#include <cycfi/touch.hpp>

namespace cycfi
{
   void main()
   {
      while (1)
      {
         debug_out(sys_tick());
         delay_ms(1000);
      }
   }
}

#include <cycfi_main_impl.hpp>
