/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#include<msp430g2553.h>
#include <cstdlib>
///////////////////////////////////////////////////////////////////////////////

#define baud_rate 1666
unsigned char TXByte;
unsigned char BitCnt;

void ConfigWDT()
{
	WDTCTL = WDTPW + WDTHOLD;
	BCSCTL1 = CALBC1_16MHZ;
	DCOCTL = CALDCO_16MHZ;
}

void ConfigPins()
{
	P1SEL |= BIT1;
	P1DIR |= BIT1 + BIT6;
	P1OUT = 0;
}

void ConfigTimerA2()
{
	CCTL0 = OUTMOD_0 + OUT;
	TACTL = TASSEL_2 + MC_2 + TAIE;
}

// Function writes Character from TXByte
void write(char data)
{
	while ( CCTL0 & CCIE );
	TXByte = data;
	BitCnt = 10;
	CCR0 += baud_rate;
	TXByte |= 0x100;
	TXByte = TXByte << 1;
	CCTL0 =  CCIS0 + OUTMOD_1 + CCIE;

}

void write_string(char const* str)
{
	while(*str)
		write(*str++);
}

void writeline_string(char const* str)
{
	while(*str)
		write(*str++);

	write('\n');
}

void writeline_digit(long val)
{
	char buff[16];
	std::ltoa(val, buff);
	writeline_string(buff);
}


int main()
{
	ConfigWDT();
	ConfigPins();
	ConfigTimerA2();
	CCTL1 = CCIE;
	CCR1 = 0;

	_EINT();


	while (1)
	{
		writeline_digit(1234);
	}

}

void uart()
{
	if (CCTL0 & CCIS0)
	{
		if ( BitCnt == 0)
			CCTL0 &= ~ CCIE ;
		else
		{
			CCTL0 |=  OUTMOD_4;
			if (TXByte & 0x01)
				CCTL0 &= ~ OUTMOD_4;
			TXByte = TXByte >> 1;
			BitCnt --;
		}
	}
}

void blink()
{
	P1OUT ^= BIT6;
}

// Timer A0 interrupt service routine
#pragma vector=TIMER0_A0_VECTOR
__interrupt void Timer_A ()
{
	CCR0 += baud_rate;
	uart();

}

#pragma vector = TIMER0_A1_VECTOR
__interrupt void Timer_1 ()
{
	switch(TAIV)
	{
		case 2:
			CCR1 += baud_rate;
			blink();
			break;
	}

}




