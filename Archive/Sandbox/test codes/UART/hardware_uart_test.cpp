#include <uart.hpp>

using namespace cycfi;

int main()
{
	WDTCTL  = WDTPW + WDTHOLD;
	BCSCTL1 = CALBC1_16MHZ;
	DCOCTL = CALDCO_16MHZ;
	TACTL = TASSEL_2 + MC_2;

	hardware_uart_setup();

  	 while(1)
  	 {
  		 if(read_char() == '1')
  			print_digit(1234);

  		 else if (read_char() == '2')
  			printline_string("Hello world!");


  	 }
}



