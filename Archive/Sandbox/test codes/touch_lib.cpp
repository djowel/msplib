#include <msp430g2553.h>
#include<touch.hpp>


#define baud_rate 1666
unsigned char TXByte;
unsigned char BitCnt;


void ConfigPins()
{
	P1SEL |= BIT1;
	P1DIR |= BIT1;
	P1OUT = 0;
}

// Function writes Character from TXByte
void write(char data)
{
	while ( CCTL0 & CCIE );
	TXByte = data;
	BitCnt = 10;
	CCR0 += baud_rate;
	TXByte |= 0x100;
	TXByte = TXByte << 1;
	CCTL0 =  CCIS0 + OUTMOD_1 + CCIE;
}

void write_string(char const* str)
{
	while(*str)
		write(*str++);
}

void writeline_string(char const* str)
{
	while(*str)
		write(*str++);

	write('\n');
}

void writeline_digit(long val)
{
	char buff[16];
	std::ltoa(val, buff);
	writeline_string(buff);
}

int main()
{
	WDTCTL = WDTPW + WDTHOLD;
	BCSCTL1 = CALBC1_16MHZ;
	DCOCTL = CALDCO_16MHZ;
	TACTL = TASSEL_2 + MC_2;
	ConfigPins();
	CCTL0 = OUTMOD_0 + OUT;
	CCTL1 = CCIE;
	CCR1 = 0;

	_EINT();

	cycfi::touch_sense t;

	while(1)
	{

		writeline_string("Hello");
	}
}

#pragma vector=TIMER0_A0_VECTOR
__interrupt void Timer_A ()
{
	CCR0 += baud_rate;
	if (CCTL0 & CCIS0)
	{
		if ( BitCnt == 0)
			CCTL0 &= ~ CCIE ;
		else
		{
			CCTL0 |=  OUTMOD_4;
			if (TXByte & 0x01)
				CCTL0 &= ~ OUTMOD_4;
			TXByte = TXByte >> 1;
			BitCnt --;
		}
	}
}

#pragma vector = PORT1_VECTOR
__interrupt void port1_ISR()
{
	P1IFG = 0;
	cycfi::timer_count = TAR - cycfi::timer_count;
	LPM0_EXIT;
}
