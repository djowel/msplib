//reference : http://dbindner.freeshell.org/msp430/cw.html
/* This triggers when a pad has been charged or discharged.  When it returns,
 * timer_count will hold the elapsed count of charging or discharging time for
 * the key.  Setup for triggering this interrupt happens in
 * measure_key_capacitance(). */

#include <msp430g2553.h>
#include <cstdlib>
#include <stdint.h>

#define PAD1 BIT4
#define PAD2 BIT5

uint32_t timer_count;
uint32_t systick();
uint32_t tar_hi = 0;

inline uint16_t capture_time();

inline void set_keys_to_low() {
	P1OUT &= ~(PAD1 + PAD2);
}

inline void charge_key(uint8_t key) {
	P1OUT |= key;
}

inline void interrupt_triggered_on_rising_edge(uint8_t key) {
	P1IES |= key; // Trigger on voltage drop.
	P1IE |= key; // Interrupt on.
	P1DIR &= ~key; // Float key and let voltage drop.
}

inline void discharge_key(uint8_t key) {
	P1IE &= ~key; // Disable interrupts on key.
	P1OUT &= ~key; // Discharge key by setting
	P1DIR |= key; // active low.
}

inline void charge_complement_line(uint8_t key) {
	P1OUT |= (PAD1 + PAD2) ^ key;
}

inline void interrupt_triggered_on_falling_edge(uint8_t key) {
	P1IES &= ~key; // Trigger on voltage rise.
	P1IE |= key; // Interrupt on.
	P1DIR &= ~key; // Float key and let voltage rise.
}

inline void discharge_complement_line(uint8_t key) {
	P1IE &= ~key; // Disable interrupts on key.
	P1OUT &= ~(PAD1 + PAD2); // Set both keys to
	P1DIR |= (PAD1 + PAD2); // active low.
}

/* Returns a value the reflects the capacitance of one of two key pads
 * connected by a large value resistor.  Assumes key to be BIT4 or BIT7. */
uint16_t measure_key_capacitance(uint8_t key) {
	set_keys_to_low();
	charge_key(key);
	interrupt_triggered_on_rising_edge(key);
	timer_count = systick(); // capture time
	__bis_SR_register( LPM0_bits + GIE);
	// enter low power mode
	discharge_key(key);

	static uint16_t sum // Save the count that was
	= timer_count; // recorded in interrupt.

	charge_complement_line(key);
	interrupt_triggered_on_falling_edge(key);

	timer_count = systick(); // capture time
	__bis_SR_register( LPM0_bits + GIE);
	// enter low power mode

	discharge_complement_line(key);

	return sum + timer_count; // Return the sum of both counts.
}

/* Computes a trimmed mean of 18 capacitance values, trimming the largest and
 * smallest samples. */
uint16_t sample_key(uint8_t key) {
	uint16_t large;
	uint16_t total;

	total = 0;

	uint16_t small = large // Measure once to initialize max
			= measure_key_capacitance(key); // and min values.

	/* Seventeen more samples happen here. Each time we decide whether the new
	 * sample is kept or if it replaces one of the extremes. */
	for (int i = 1; i < 32; i++) {
		uint16_t cap = measure_key_capacitance(key);

		if (cap < small) {
			total += small;
			small = cap;
		}

		else if (cap > large) {
			total += large;
			large = cap;
		}

		else {
			total += cap;
		}

		for (int j = 0; j < (cap & 0x0F); j++)
			;

	}

	/* We average 16 values (not including the two extremes) */
	return total >> 4;
}

#define baud_rate 833
unsigned char TXByte;
unsigned char BitCnt;

void ConfigPins() {
	P1SEL |= BIT1;
	P1DIR |= BIT1;
	P1OUT = 0;
}

// Function writes Character from TXByte
void write(char data) {
	while (CCTL0 & CCIE)
		;
	TXByte = data;
	BitCnt = 10;
	CCR0 += baud_rate;
	TXByte |= 0x100;
	TXByte = TXByte << 1;
	CCTL0 = CCIS0 + OUTMOD_1 + CCIE;
}

void write_string(char const* str) {
	while (*str)
		write(*str++);
}

void writeline_string(char const* str) {
	while (*str)
		write(*str++);

	write('\n');
}

void writeline_digit(long val) {
	char buff[16];
	std::ltoa(val, buff);
	writeline_string(buff);
}

void tar_interrupt() {
	tar_hi++;
}

uint32_t systick() {
	return TAR | (uint32_t(tar_hi) << 16);
}

int main() {
	WDTCTL = WDTPW + WDTHOLD;
	BCSCTL1 = CALBC1_16MHZ;
	DCOCTL = CALDCO_16MHZ;
	TACTL = MC_2 + TASSEL_2 + ID_1 + TAIE;
	ConfigPins();
	CCTL0 = OUTMOD_0 + OUT;
	CCTL1 = CCIE;
	CCR1 = 0;

	P1DIR |= PAD2 + PAD1 + BIT6;
	_EINT();
	while (1) {
		WDTCTL = WDTPW + WDTHOLD; // hold watchdog clock

		writeline_digit(sample_key(PAD2)); // print value
		__delay_cycles(1000);

		WDTCTL = WDT_MDLY_0_064; // watchdog timer interval at 0.064ms
		IE1 |= WDTIE; // enable WDT interrupt

		__bis_SR_register( LPM0_bits + GIE);
		// enter low power mode
	}
}

#pragma vector = WDT_VECTOR
__interrupt void wdt_isr() {
	__bic_SR_register_on_exit( LPM0_bits);
}

#pragma vector = PORT1_VECTOR
__interrupt void port1_isr() {
	P1IFG = 0;
	timer_count = systick() - timer_count;
	__bic_SR_register_on_exit( LPM0_bits);
}

#pragma vector=TIMER0_A0_VECTOR
__interrupt void Timer_A() {
	CCR0 += baud_rate;
	if (CCTL0 & CCIS0) {
		if (BitCnt == 0)
			CCTL0 &= ~CCIE;
		else {
			CCTL0 |= OUTMOD_4;
			if (TXByte & 0x01)
				CCTL0 &= ~OUTMOD_4;
			TXByte = TXByte >> 1;
			BitCnt--;
		}
	}
}

#pragma vector = TIMER0_A1_VECTOR
__interrupt void TIMER0_A1_ISR() {
	switch (TAIV) {
	case 2:
		break;
	case 4:
		break;
	case 10:
		tar_interrupt();
		TACTL &= ~TAIFG;
		break;

	}
}

