//reference : http://dbindner.freeshell.org/msp430/cw.html
/* This triggers when a pad has been charged or discharged.  When it returns,
 * timer_count will hold the elapsed count of charging or discharging time for
 * the key.  Setup for triggering this interrupt happens in
 * measure_key_capacitance(). */

#include <msp430g2553.h>
#include <cstdlib>
#include <stdint.h>

#define RXD BIT3
#define TXD BIT2

#define PAD1 BIT4
#define PAD2 BIT5

volatile uint16_t timer_count;

inline void set_keys_to_low()
{
   P1OUT &= ~(PAD1 + PAD2);
}

inline void charge_key(uint8_t key)
{
   P1OUT |= key;
}

inline void interrupt_triggered_on_rising_edge(uint8_t key)
{
   P1IES |= key;                              // Trigger on voltage drop.
   P1IE |= key;                               // Interrupt on.
   P1DIR &= ~key;                             // Float key and let voltage drop.
}

inline void discharge_key(uint8_t key)
{
   P1IE &= ~key;                              // Disable interrupts on key.
   P1OUT &= ~key;                             // Discharge key by setting
   P1DIR |= key;                              // active low.
}

inline void charge_complement_line(uint8_t key)
{
   P1OUT |= (PAD1 + PAD2)^key;
}

inline void interrupt_triggered_on_falling_edge(uint8_t key)
{
   P1IES &= ~key;                             // Trigger on voltage rise.
   P1IE |= key;                               // Interrupt on.
   P1DIR &= ~key;                             // Float key and let voltage rise.
}

inline void discharge_complement_line(uint8_t key)
{
   P1IE &= ~key;                              // Disable interrupts on key.
   P1OUT &= ~(PAD1 + PAD2);                   // Set both keys to
   P1DIR |=  (PAD1 + PAD2);                   // active low.
}

//serial
void uart_setup()
{
   P1SEL  = RXD + TXD;
   P1SEL2 = RXD + TXD;
   UCA0CTL1 |= UCSSEL_2;                      // SMCLK
   UCA0BR0 = 104;                             // 1MHz 9600
   UCA0BR1 = 0;                               // 1MHz 9600
   UCA0MCTL = UCBRS0;                         // Modulation UCBRSx = 1
   UCA0CTL1 &= ~UCSWRST;                      // Initialize USCI state machine
   TACTL = TASSEL_2 + MC_2;
}

inline bool is_txbuff_ready()
{
   return !(IFG2&UCA0TXIFG);
}

void write_char(int8_t c)                     // write char
{
   IE2 |= UCA0TXIE;                           // enable TX interrupt

   while (is_txbuff_ready())                  // USCI_A0 TX buffer ready?
      ;
      UCA0TXBUF =  c;
}

void printline_string(const char* str)        // print string + new line
{
   while (*str)
      write_char(*str++);

   write_char(0x0A);
}

void print_int(uint32_t i)                    // print int16_t + new line
{
   char buffer[16];
   int16_t ptr = std::ltoa(i, buffer);
   printline_string(buffer);
}

/* Returns a value the reflects the capacitance of one of two key pads
 * connected by a large value resistor.  Assumes key to be BIT4 or BIT7. */
uint16_t measure_key_capacitance( uint8_t key )
{
   set_keys_to_low();
   charge_key(key);
   interrupt_triggered_on_rising_edge(key);
   timer_count = CCR1;                         // capture time
   __bis_SR_register( LPM0_bits + GIE );      // enter low power mode
   discharge_key(key);

   static uint16_t sum                        // Save the count that was
      = timer_count;                          // recorded in interrupt.

   charge_complement_line(key);
   interrupt_triggered_on_falling_edge(key);
   
   timer_count = CCR1;                         // capture time
   __bis_SR_register( LPM0_bits + GIE );      // enter low power mode
   
   discharge_complement_line(key);

   return sum + timer_count;                  // Return the sum of both counts.
}

/* Computes a trimmed mean of 18 capacitance values, trimming the largest and
 * smallest samples. */
uint16_t sample_key(uint8_t key)
{
   uint16_t large;
   uint16_t total;

   total = 0;

   uint16_t small = large                     // Measure once to initialize max
      = measure_key_capacitance( key );       // and min values.

   /* Seventeen more samples happen here. Each time we decide whether the new
    * sample is kept or if it replaces one of the extremes. */
   for (int i = 1; i < 18; i++ )
   {
      uint16_t cap = measure_key_capacitance(key);

      if (cap < small)
      {
         total += small;
         small = cap;
      }

      else if (cap > large)
      {
         total += large;
         large = cap;
      }

      else
      {
         total += cap;
      }

      for (int j=0; j < (cap&0x0F); j++ );

   }

   /* We average 16 values (not including the two extremes) */
   return total >> 4;
}

int main()
{
   WDTCTL = WDTPW + WDTHOLD;
   BCSCTL1 = CALBC1_1MHZ;
   DCOCTL = CALDCO_1MHZ;
   TACTL = MC_2 + TASSEL_2;
   CCTL1 = CCIE;
   CCR1 = 0;
   
   _EINT();

   P1DIR |= PAD2 + PAD1;
   uart_setup();
   
   uint16_t baseline1=0, baseline2=0, samp1, samp2;

   /* Get maximum baseline values for each key.  In the main loop, more than
     * 125% of the baseline value will indicate a touch event. */

   for (int16_t i = 0; i < 32; i++)
   {
      samp1 = sample_key(PAD2);

      if (samp1 > baseline1)
         baseline1 = samp1;

      samp2 = sample_key(PAD1);

      if (samp2 > baseline2)
         baseline2 = samp2;
   }

   while(1)
   {
      WDTCTL = WDTPW + WDTHOLD;               // hold watchdog clock

      samp1 = sample_key(PAD2);
      samp2 = sample_key(PAD1);

      print_int(1234);                       // print value
      __delay_cycles(100);
   }
}

#pragma vector = PORT1_VECTOR
__interrupt void port1_isr()
{
   P1IFG = 0;
   timer_count = CCR1 - timer_count;
   __bic_SR_register_on_exit( LPM0_bits );
}

#pragma vector = USCIAB0TX_VECTOR;
__interrupt void USCIA0TX_ISR (void)
{
   IE2 &= ~UCA0TXIE;                          // disable interrupt of TX
}

#pragma vector = TIMER0_A1_VECTOR
__interrupt void timer0_A1_ISR()
{
	switch(TA0IV)
	{
		case 2:
			TA0CCR1 += 104;
			break;

	}
}


