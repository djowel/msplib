#include <msp430g2553.h>

#define KEY_LVL     1000                     // Defines threshold for a key press

/*Set to ~ half the max delta expected*/

#define LED_1   (0x01)                      // P1.0 LED output
#define LED_2   (0x40)                      // P1.6 LED output

unsigned int base_cnt,
			 meas_cnt;

int delta_cnt;
unsigned char key_press;
char key_pressed;
int cycles;
const unsigned char electrode_bit= BIT0;

void measure_count(void);                   // Measures each capacitive sensor
void pulse_LED(void);                       // LED gradient routine

int main()
{
	unsigned int i, j;
	WDTCTL = WDTPW + WDTHOLD;                 // Stop watchdog timer
	BCSCTL1 = CALBC1_1MHZ;                    // Set DCO to 1, 8, 12 or 16MHz
	DCOCTL = CALDCO_1MHZ;
	BCSCTL2 |= DIVS_2;                        // SMCLK/(0:1,1:2,2:4,3:8)
	BCSCTL1 |= DIVA_1;                        // ACLK/(0:1,1:2,2:4,3:8)
	BCSCTL3 |= XCAP_1;                        // Configure Load Caps
	IE1 |= WDTIE;                             // enable WDT interrupt
	P1DIR = LED_1 + LED_2;                    // P1.0 & P1.6 = LEDs
	P1OUT = 0x00;

	do
	{
		IFG1 &= ~OFIFG;                         // Clear OSCFault flag
	    for (i = 0xFF; i > 0; i--);             // Time for flag to set
	}

	while (IFG1 & OFIFG);                     // OSCFault flag still set?

	__bis_SR_register(GIE);                  // Enable interrupts

	measure_count();                          // Establish baseline capacitance
	base_cnt = meas_cnt;
	for(i=15; i>0; i--)                       // Repeat and avg base measurement
	{
		measure_count();
		base_cnt = (meas_cnt+base_cnt)/2;
	}

	while (1)
	{
		j = KEY_LVL;
		key_pressed = 0;                            // Assume no keys are pressed

	    measure_count();                            // Measure all sensors
	    delta_cnt = base_cnt - meas_cnt;            // Calculate delta: c_change

	      /* Handle baseline measurment for a base C decrease*/
	      if (delta_cnt < 0)                        // If negative: result increased
	      {                                         // beyond baseline, i.e. cap dec
	          base_cnt = (base_cnt+meas_cnt) >> 1;  // Re-average quickly
	          delta_cnt = 0;                 		// Zero out for pos determination
	      }
	      if (delta_cnt > j)                        // Determine if each key is pressed
	      {                                         // per a preset threshold
	        key_press = 1;                          // Specific key pressed
	        j = delta_cnt;
	        key_pressed = 1;                        // key pressed
	      }
	      else
	        key_press = 0;


	    /* Delay to next sample, sample more slowly if no keys are pressed*/
	    if (key_pressed)
	    {
	      BCSCTL1 = (BCSCTL1 & 0x0CF) + DIVA_0; // ACLK/(0:1,1:2,2:4,3:8)
	      cycles = 20;
	    }
	    else
	    {
	      cycles--;
	      if (cycles > 0)
	        BCSCTL1 = (BCSCTL1 & 0x0CF) + DIVA_0; // ACLK/(0:1,1:2,2:4,3:8)
	      else
	      {
	        BCSCTL1 = (BCSCTL1 & 0x0CF) + DIVA_3; // ACLK/(0:1,1:2,2:4,3:8)
	        cycles = 0;
	      }
	    }

	    /* Handle baseline measurement for a base C increase*/
	    if (!key_pressed)                       // Only adjust baseline down
	    {                                       // if no keys are touched
	        base_cnt = base_cnt - 1;      // Adjust baseline down, should be
	    }                                       // slow to accomodate for genuine
	     pulse_LED();                           // changes in sensor C

	    __delay_cycles(20000);
	}
}

void measure_count(void)
{
  unsigned int i, j;
  _DINT();                              // Disable interrupts
  BCSCTL1 = (BCSCTL1 & 0x0CF) + DIVA_3; // ACLK/(0:1,1:2,2:4,3:8)

    // Configure Ports for relaxation oscillator
    P2DIR &= ~ electrode_bit;        //
    P2SEL &= ~ electrode_bit;        //
    P2SEL2 |= electrode_bit;         // Set target Pin Oscillator
    TA0CTL = TASSEL_3 + MC_2 + TACLR;   // PinOsc Clock source, cont mode
    TA0CCTL0 = CM_1 + CCIS_1 + CAP;     // Capture on Pos Edges, ACLK, Cap, Interrupt
    TA0CCTL0 |= CCIE;                   // Enable Interrupt

    __bis_SR_register(LPM3_bits+GIE);   // Wait for TIMER interrupt
    __bis_SR_register(LPM3_bits+GIE);   // Wait for TIMER interrupt
    meas_cnt = TACCR0;               // Save result
    for (j=0;j<15;j++) {
      __bis_SR_register(LPM3_bits+GIE); // Wait for TIMER interrupt
    }
    TA0CTL &= MC_2;                     // Halt Timer
    TA0CCTL0 &= ~CCIE;                  // Disable Interrupt
    meas_cnt = TACCR0 - meas_cnt; // Save Measured
    P2SEL2 &= ~electrode_bit;        // Clear target Pin Oscillator

  BCSCTL1 = (BCSCTL1 & 0x0CF) + DIVA_0; // ACLK/(0:1,1:2,2:4,3:8)
}

void pulse_LED(void)
{
  switch (key_pressed){
  case 0: P1OUT &= ~(LED_1 + LED_2);
          break;
  case 1: P1OUT ^= LED_1 + LED_2;
          break;


    }
}
/* Timer A1 interrupt service routine*/
#pragma vector=TIMER0_A0_VECTOR
__interrupt void Timer_A0 (void)
{
   __bic_SR_register_on_exit(LPM3_bits);     // Exit LPM3 on reti
}

