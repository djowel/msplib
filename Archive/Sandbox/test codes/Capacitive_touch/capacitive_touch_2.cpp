//reference : http://dbindner.freeshell.org/msp430/cw.html
/* This triggers when a pad has been charged or discharged.  When it returns,
 * timer_count will hold the elapsed count of charging or discharging time for
 * the key.  Setup for triggering this interrupt happens in
 * measure_key_capacitance(). */

#include <msp430g2553.h>
#include <cstdlib>
#include <stdint.h>

#define RED BIT0
#define GRN BIT6

#define DOT BIT4
#define DASH BIT5

volatile uint16_t timer_count;

inline void enter_sleep_mode()
{
    __bis_SR_register( LPM0_bits + GIE ); 
}

inline void capture_timer_count()
{
   timer_count = TAR;
}

inline void set_keys_to_low()
{
   P1OUT &= ~(BIT4 + BIT5); 
}

inline void charge_key(uint16_t key)
{
   P1OUT |= key;                              
}

inline void interrupt_triggered_on_rising_edge(uint16_t key)
{
   P1IES |= key;                              // Trigger on voltage drop.
   P1IE |= key;                               // Interrupt on.
   P1DIR &= ~key;                             // Float key and let voltage drop.
}

inline void discharge_key(uint16_t key)
{
   P1IE &= ~key;                              // Disable interrupts on key.
   P1OUT &= ~key;                             // Discharge key by setting
   P1DIR |= key;                              // active low.
}

inline void charge_complement_line(uint16_t key)
{
   P1OUT |= (BIT4 + BIT5)^key;
}

inline void interrupt_triggered_on_falling_edge(uint16_t key)
{
   P1IES &= ~key;                             // Trigger on voltage rise.
   P1IE |= key;                               // Interrupt on.
   P1DIR &= ~key;                             // Float key and let voltage rise.
}

inline void discharge_complement_line(uint16_t key)
{
   P1IE &= ~key;                              // Disable interrupts on key.
   P1OUT &= ~(BIT4 + BIT5);                   // Set both keys to
   P1DIR |=  (BIT4 + BIT5);                   // active low.
}

inline void add_jitter(int16_t cap)
{
   for (int16_t j=0; j < (cap&0x0F); j++ ); 
}

inline void initial_setup()
{
   WDTCTL = WDTPW + WDTHOLD;
   BCSCTL1 = CALBC1_1MHZ;
   DCOCTL = CALDCO_1MHZ;
   TACTL = MC_2 + TASSEL_2;
}

inline void pin_setup()
{
   P1DIR |= RED + GRN;
   P1DIR |= DASH + DOT;
}

inline void watch_dog_timer_interval_at_0_5_ms()
{
   WDTCTL = WDT_MDLY_0_064;
   IE1 |= WDTIE;
}

/* Returns a value the reflects the capacitance of one of two key pads
 * connected by a large value resistor.  Assumes key to be BIT4 or BIT7. */
uint16_t measure_key_capacitance( uint16_t key ) 
{
   set_keys_to_low();
   charge_key(key);
   interrupt_triggered_on_rising_edge(key);
   capture_timer_count();  
   enter_sleep_mode(); 
   discharge_key(key);

   static uint16_t sum                        // Save the count that was 
      = timer_count;                          // recorded in interrupt.

   charge_complement_line(key);               
   interrupt_triggered_on_falling_edge(key);
   capture_timer_count();   
   enter_sleep_mode();  
   discharge_complement_line(key);

   return sum + timer_count;                  // Return the sum of both counts.
}

/* Computes a trimmed mean of 18 capacitance values, trimming the largest and
 * smallest samples. */
uint16_t sample_key( unsigned char key ) 
{
   int16_t large;
   int32_t total;

   total = 0;

   int16_t small = large                      // Measure once to initialize max 
      = measure_key_capacitance( key );       // and min values.

   /* Seventeen more samples happen here. Each time we decide whether the new
    * sample is kept or if it replaces one of the extremes. */
   for (int16_t i = 1; i < 18; i++ ) 
   {
      int16_t cap = measure_key_capacitance(key);
      
      if (cap < small) 
      {
         total += small;
         small = cap;
      } 
      
      else if (cap > large) 
      {
         total += large;
         large = cap;
      } 
      
      else 
      {
         total += cap;
      }

      add_jitter(cap);
      
   }

   /* We average 16 values (not including the two extremes) */
   return total >> 4;
}

int main() 
{    
   initial_setup();
   pin_setup();
   int16_t baseline1=0, baseline2=0, samp1, samp2;

   /* Get maximum baseline values for each key.  In the main loop, more than
     * 125% of the baseline value will indicate a touch event. */
     
   P1OUT |= RED + GRN;                // Visually signal the calibration cycle.
   
   for (int16_t i = 0; i < 32; i++) 
   {      
      samp1 = sample_key(DASH);
      
      if (samp1 > baseline1)
         baseline1 = samp1;
      
      samp2 = sample_key(DOT);
        
      if (samp2 > baseline2) 
         baseline2 = samp2;
   }
   
   P1OUT &= ~(RED + GRN);                     // Lights off, calibration done.

   while (1) 
   {
      WDTCTL = WDTPW + WDTHOLD;               // hold watchdog clock

      samp1 = sample_key(DASH);
      samp2 = sample_key(DOT);

      /* One paddle lights the green light. */
      if (4*samp1 > 5*baseline1)       
         P1OUT |= GRN;        
      else 
         P1OUT &= ~GRN;
        
      /* The other paddle lights the red. */
      if (4*samp2 > 5*baseline2)      
         P1OUT |= RED;   
      else 
         P1OUT &= ~RED;
         
      watch_dog_timer_interval_at_0_5_ms();     
      enter_sleep_mode();
   }
}

#pragma vector = WDT_VECTOR
__interrupt void wdt_isr()
{
	__bic_SR_register_on_exit( LPM0_bits );
}

#pragma vector = PORT1_VECTOR
__interrupt void port1_isr()
{
   P1IFG = 0;
   timer_count = TAR - timer_count;
   __bic_SR_register_on_exit( LPM0_bits );
}


