#include <msp430g2553.h>
#include <stdint.h>

#define PAD1 BIT4
#define PAD2 BIT5

uint32_t tar_count;
uint32_t timer_count;

uint32_t systick()
{
   return TAR | (uint32_t(tar_count) << 16);
}

void tar_interrupt()
{
   tar_count++;
}

uint32_t measure_pad(uint8_t pad)
{
   uint32_t capacitance_value;
   
   P1OUT &= ~ PAD1 + PAD2;
   P1OUT |= pad;
   P1IES |= pad;
   P1IE |= pad;
   P1DIR &= ~ pad;
   timer_count = systick();
   LMP0;
   P1IE &= ~pad;
   P1OUT &= ~pad;
   P1DIR |= pad;
   
   capacitive_value = timer_count;
   P1OUT |= (PAD1 + PAD2)^pad;
   P1IES &= ~pad;
   P1IE |= pad;
   P1DIR &= ~pad;
   timer_count = systick();
   P1IE &= ~pad;
   P1OUT &= ~(PAD1 + PAD2);
   P1DIR |= (PAD1 + PAD2);
   
   return capacitance_value + timer_count;  
}

int main()
{
   WDTCTL = WDTPW + WDTHOLD;
   BCSCTL1 = CALBC1_16MHZ;
   DCOCTL = CALDC0_16MHZ;
   TACTL = TASSEL_2 + MC_2 + TAIE;
   
   CCTL1 = CCIE;
   CCR1 = 0;
   
   _EINT();
}

#pragma vector = PORT1_VECTOR
__interrupt void PORT1_ISR()
{
   P1IFG = 0;
   timer_count = systick() - timer_count;
   LPM0_EXIT;
}

#pragma vector = TIMER0_A1_VECTOR
__interrupt void TIMER0_A1_ISR(0
{
   switch(TA0IV)
   {
      case 2: 
         break;
      
      case 4: 
         break;
      
      case 10: 
         tar_interrupt();
         TACTL &= ~TAIFG;
         break;
   }
}