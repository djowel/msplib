#include <msp430g2553.h>
#include <cstdlib>
#include <stdint.h>

#define RXD BIT3
#define TXD BIT2

void initial_setup()
{
   WDTCTL = WDTPW + WDTHOLD;                    // Stop watchdog timer
   BCSCTL1 = CALBC1_1MHZ;                       // Set DCO to 1, 8, 12 or 16MHz
   DCOCTL = CALDCO_1MHZ;
   BCSCTL3 |= XCAP_1;                           // Configure Load Caps
   IE1 |= WDTIE;                                // enable WDT interrupt
}

void uart_setup()
{     
   P1SEL  = RXD + TXD;
   P1SEL2 = RXD + TXD;
   UCA0CTL1 |= UCSSEL_2;                        // SMCLK
   UCA0BR0 = 104;                               // 1MHz 9600
   UCA0BR1 = 0;                                 // 1MHz 9600
   UCA0MCTL = UCBRS0;                           // Modulation UCBRSx = 1
   UCA0CTL1 &= ~UCSWRST;                        // Initialize USCI state machine
   TACTL = TASSEL_2 + MC_2;
}

inline bool is_txbuff_ready()
{
   return !(IFG2&UCA0TXIFG);
}

void write_char(int8_t c)                       // write char
{
   IE2 |= UCA0TXIE;                             // enable TX interrupt

   while (is_txbuff_ready())                    // USCI_A0 TX buffer ready?
      ;                                     
      UCA0TXBUF =  c;
}

void printline_string(const char* str)          // print string + new line
{
   while (*str)
      write_char(*str++);
   
   write_char(0x0A);
}

void print_int(uint32_t i)                      // print int + new line
{
   char buffer[16];
   int ptr = std::ltoa(i, buffer);
   printline_string(buffer);
}

inline void capture_data()
{
	for (int j = 0; j < 15; j++)
      __bis_SR_register(LPM3_bits+GIE);         // Wait for TIMER interrupt
}
   
unsigned int measure_count(const uint8_t pin)
{
   _DINT();                                     // Disable interrupts
   BCSCTL1 = (BCSCTL1 & 0x0CF) + DIVA_3;        // ACLK/(0:1,1:2,2:4,3:8)

   P2DIR &= ~ pin;                              //
   P2SEL &= ~ pin;                              //
   P2SEL2 |= pin;                               // Set target Pin Oscillator
   TA0CTL = TASSEL_3 + MC_2 + TACLR;            // PinOsc Clock source, cont mode
   TA0CCTL0 = CM_1 + CCIS_1 + CAP;              // Capture on Pos Edges, ACLK, Cap, Interrupt
   TA0CCTL0 |= CCIE;                            // Enable Interrupt
   
   __bis_SR_register(LPM3_bits+GIE);            // Wait for TIMER interrupt
   __bis_SR_register(LPM3_bits+GIE);            // Wait for TIMER interrupt   
   
   uint16_t meas_cnt = TACCR0;                  // Save result
   capture_data();

   TA0CTL &= MC_2;                              // Halt Timer
   TA0CCTL0 &= ~CCIE;                           // Disable Interrupt
   meas_cnt += TACCR0;                          // Save Measured
   P2SEL2 &= ~pin;                              // Clear target Pin Oscillator

   BCSCTL1 = (BCSCTL1 & 0x0CF) + DIVA_0;        // ACLK/(0:1,1:2,2:4,3:8)

   return meas_cnt;
}

inline void osc_flag_clear()
{
   IFG1 &= ~OFIFG; 
}

inline bool is_osc_flag_clear()
{
   return IFG1 & OFIFG;
}

int main()
{  
   initial_setup();
   uart_setup();

   do
   {
      osc_flag_clear();                         // Clear OSCFault flag
      for (int i = 0xFF; i > 0; i--);           // Time for flag to set
   }

   while (is_osc_flag_clear())                  // OSCFault flag still set?
      ;                        

   __bis_SR_register(GIE);                      // Enable interrupts

   uint16_t base_cnt = measure_count(BIT3);

   for (int i = 15; i > 0; i--)                 // Repeat and avg base measurement
      base_cnt = (measure_count(BIT3) +base_cnt)/2;
   

   while (1)
   {
	   uint16_t delta_cnt
         = base_cnt - measure_count(BIT3);      // Calculate delta: c_change
         
      print_int(delta_cnt);                     // print value
      __delay_cycles(20000);
   }
}

/* Timer A1 interrupt service routine*/
#pragma vector=TIMER0_A0_VECTOR
__interrupt void Timer_A0 (void)
{
   __bic_SR_register_on_exit(LPM3_bits);        // Exit LPM3 on reti
}

#pragma vector = USCIAB0TX_VECTOR;
__interrupt void USCIA0TX_ISR (void)
{
   IE2 &= ~UCA0TXIE;                            // disable interrupt of TX
}

