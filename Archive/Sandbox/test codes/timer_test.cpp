/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#include <cycfi.hpp>
#include <cycfi/debug.hpp>
#include <timer.hpp>

namespace cycfi
{
   template <>
   struct task<2> : task_base<2>
   {
      static uint16_t const period = 16000;

      task() : led(out), i(0)
      {
         led = 1;
      }

      void perform()
      {
         if (i++ == 500)
         {
            led = !led;
            i = 0;
         }
      }

      port_a0 led;  // port 1.0
      int i;
   };

   void main_loop()
   {
	   while (1)
	   {
		 stop_watch t;
		 t.start();
		 delay_ms(1000);
		 debug_digit_out(t.elapsed_time());
         debug_out('\n');
	   }
   }
}

#include <cycfi_main.ipp>
