#include <msp430.h>
#include <stdint.h>
#include <cstdlib>

volatile uint16_t timer_count;

#define A BIT4
#define B BIT5

uint16_t measure()
{
   P1OUT &= ~(A + B);                     // set keys to low
   __delay_cycles(16);                    // wait a bit

   P1OUT |= A;                            // charge A
   __delay_cycles(16);                    // wait a bit

   P1IES |= A;                            // Trigger on falling edge.
   P1IE |= A;                             // Interrupt on.
   P1DIR &= ~A;                           // Float A and let voltage drop.

   timer_count = TAR;                     // capture timer count
   __bis_SR_register(LPM0_bits + GIE);    // sleep

   // discharge A
   P1IE &= ~A;                            // Disable interrupts on A.
   P1OUT &= ~A;                           // Discharge A by setting
   P1DIR |= A;                            // active low.

   uint16_t sum = timer_count;            // Save the current count

   P1OUT |= B;                            // charge complement line
   __delay_cycles(16);                    // wait a bit

   P1IES &= ~A;                           // Trigger on rising edge.
   P1IE |= A;                             // Interrupt on.
   P1DIR &= ~A;                           // Float A and let voltage rise.

   timer_count = TAR;                     // capture timer count
   __bis_SR_register(LPM0_bits + GIE);    // sleep

   // discharge complement line
   P1IE &= ~A;                            // Disable interrupts on A.
   P1OUT &= ~(A + B);                     // Set both keys to
   P1DIR |= (A + B);                      // active low.

   return sum + timer_count;              // Return the sum of both counts.
}


//*********************************************************************//

void uart_setup()
{
   P1SEL  = BIT1 + BIT2;
   P1SEL2 = BIT1 + BIT2;
   UCA0CTL1 |= UCSSEL_2;                  // SMCLK
   UCA0BR0 = 130;                         // 16MHz 9600
   UCA0BR1 = 6;                           // 16MHz 9600
   UCA0MCTL = UCBRS0;                     // Modulation UCBRSx = 1
   UCA0CTL1 &= ~UCSWRST;                  // Initialize USCI state machine

   __bis_SR_register(GIE);
}

void write(uint8_t data)
{
   IE2 |= UCA0TXIE;                       // enable TX interrupt
   while (!(IFG2&UCA0TXIFG))              // USCI_A0 TX buffer ready?
      ;
   UCA0TXBUF =  data;
}

//*********************************************************************//

inline void delay_ms(uint16_t ms)
{
   while (--ms)
      _delay_cycles(16000);
}

void print_values(uint16_t a, uint16_t b, uint16_t c, uint16_t d)
{
   // offset for signed printing
   a -= 32768;
   b -= 32768;
   c -= 32768;
   d -= 32768;

   write(0xAB);         // magic lo
   write(0xCD);         // magic hi
   write(8);            // payload lo
   write(0);            // payload hi
   write(a & 0xFF);     // lsb
   write(a >> 8);       // msb
   write(b & 0xFF);     // lsb
   write(b >> 8);       // msb
   write(c & 0xFF);     // lsb
   write(c >> 8);       // msb
   write(d & 0xFF);     // lsb
   write(d >> 8);       // msb
}

int main()
{
   // basic setup 16MHZ
   WDTCTL = WDTPW + WDTHOLD;
   BCSCTL1 = CALBC1_16MHZ;
   DCOCTL = CALDCO_16MHZ;
   TACTL = MC_2 + TASSEL_2 + TAIE;

   // pin setup
   P1DIR |= B + A;
   uart_setup();
   CCTL0 = OUTMOD_0 + OUT;

   _EINT();


   uint16_t prev = measure();
   uint16_t filtered = prev;
   uint16_t peak = prev;
   uint16_t filtered_diff = 0;
   uint16_t filtered_diff2 = 0;
   uint16_t filtered_diff3 = 0;
   while (1)
   {
      uint16_t current = measure();
      filtered = (filtered + current) / 2;
      if (current > peak)
      {
         peak = current;   // reset peak
      }
      else
      {
         if (peak > 1024)
            peak -= 1024;      // decay peak
         else
            peak = 0;
      }

      uint16_t diff = (current > prev) ? (current - prev) : (prev - current);
      filtered_diff = (filtered_diff + diff) / 2;
      filtered_diff2 = (filtered_diff2 + filtered_diff) / 2;
      filtered_diff3 = (filtered_diff3 + filtered_diff2) / 2;

      print_values(current, filtered, diff, filtered_diff2);

      prev = current;
      delay_ms(3);
   }
}

//*********************************************************************//

#pragma vector = PORT1_VECTOR
__interrupt void port1_isr()
{
   P1IFG = 0;
   timer_count = TAR - timer_count;
   __bic_SR_register_on_exit( LPM0_bits );
}

#pragma vector = TIMER0_A1_VECTOR
__interrupt void TIMER0_A1_ISR()
{
   switch(TAIV)
   {
      case 2:
         break;
      case 4:
         break;
      case 10:
         TACTL &= ~TAIFG;
         break;

   }
}

#pragma vector = USCIAB0RX_VECTOR;
__interrupt void USCIA0RX_ISR (void)
{
   IE2 &= ~UCA0RXIE;                         // disable interrupt of RX
}

#pragma vector = USCIAB0TX_VECTOR;
__interrupt void USCIA0TX_ISR (void)
{
   IE2 &= ~UCA0TXIE;                         // disbale interrupt of TX
}

