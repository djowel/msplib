/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#include <msp430.h>
#include <cycfi/uart.hpp>

namespace cycfi
{
   namespace
   {
      port_bit<1, 4> tx_port(1);  // port 1.1
      uint16_t tx_byte;
      uint8_t bit_cnt;
   }

   void uart_setup()
   {
      bit_cnt = 0;
      tx_port = 1;
      disable_timer_interrupt1();
   }

   void schedule_1()
   {
      if (bit_cnt)
      {
         switch (bit_cnt)
         {
            case 10: // start
               tx_port = 0;
               break;

            default:
               tx_port = tx_byte & 0x1;
               tx_byte >>= 1;
               break;

            case 1:  // stop
               tx_port = 1;
             break;
         }
         bit_cnt--;
      }
   }

   void uart_write(uint8_t data)
   {
      while (bit_cnt)
         ;

      tx_byte = data;
      bit_cnt = 10;
      enable_timer_interrupt1();
   }
}



