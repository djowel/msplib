/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#include <cycfi/main.hpp>
#include <cycfi/config.hpp>

#if (CYCFI_LIB_MCU == MSP430)

int main()
{
   WDTCTL = WDTPW + WDTHOLD;  // Stop WDT
   TACTL = TASSEL_2 + MC_2 + TAIE;
//   WDTCTL = WDT_MDLY_0_064;
//   IE1 |= WDTIE;
   CCTL0 = CCIE;              // CCR0 interrupt enabled
   CCTL1 = CCIE;              // CCR0 interrupt enabled
   CCR0 = 0;
   CCR1 = 0;

   cycfi::setup();

   _BIS_SR(LPM0_bits + GIE);  // Enable the global interrupt and enter LPM0}
}

#pragma vector=TIMER0_A0_VECTOR
__interrupt void TimerA0_ISR()
{
   CCR0 += cycfi::interrupt_period1;
   cycfi::schedule_1();
}

#pragma vector=TIMER0_A1_VECTOR
__interrupt void TimerA1_ISR()
{
   switch (TAIV)
   {
      case 2:
         CCR1 += cycfi::interrupt_period2;
         cycfi::schedule_2();
         break;
   }
}

#pragma vector = WDT_VECTOR
__interrupt void WDT_ISR()
{
//	int x = 5000;
//	if(--x==0)
//	{
//		cycfi::wdt_schedule();
//		x=5000;
//	}
}


#endif
