/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#include <cycfi/core.hpp>
#include <cycfi/main.hpp>
#include <msp430g2231.h>

namespace cycfi
{
   port_bit<1, 0> led(1);  // port1 bit 1
   port_bit<1, 6> led2(1);

   void setup()
   {
   }

   void schedule_2()
   {
	  led2 = 1;
	  led = 1;
      while(1)
      {
         delay_ms(1000);
         led = !led;
         led2 = !led2;
      }

   }

   void schedule_1()
   {

   }
}

