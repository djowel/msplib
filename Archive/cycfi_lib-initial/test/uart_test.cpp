/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#include <msp430g2231.h>
#include <cycfi/uart.hpp>
#include <cycfi/core.hpp>

namespace cycfi
{
   port_bit<1, 6> led2(1);  // port 1.6

   void setup()
   {
      uart_setup();
   }

   void schedule_2()
   {
      static int i = 0;

      if (i++ == 100)
      {
         led2 = !led2;
         i = 0;

         uart_write('K');
      }
   }

   void wdt_schedule()
   {


   }
}

