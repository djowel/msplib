/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#include <cycfi/core.hpp>

namespace cycfi
{
   void uart_setup();
   void uart_write(uint8_t data);
}

