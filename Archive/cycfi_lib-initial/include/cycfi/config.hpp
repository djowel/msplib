/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#if !defined(CONFIG_H_SEPTEMBER_14_2012)
#define CONFIG_H_SEPTEMBER_14_2012
#include <stdint.h>

namespace cycfi
{
   uint32_t const clock_speed = 1000000;
}

// Define for the MCU used
#define CYCFI_LIB_MCU MSP430
// #define CYCFI_LIB_MCU PICCOLO

#if (CYCFI_LIB_MCU == MSP430)
# include <msp430.h>
#endif

#endif

