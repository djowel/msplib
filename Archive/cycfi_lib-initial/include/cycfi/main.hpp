/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#if !defined(MAIN_HPP_SEPTEMBER_14_2012)
#define MAIN_HPP_SEPTEMBER_14_2012

#include <cycfi/config.hpp>

namespace cycfi
{
   void setup();

   uint16_t const interrupt_period1 = 104;
   uint16_t const interrupt_period2 = 1000;

   void schedule_1();
   void schedule_2();
   void wdt_schedule();
}

#endif
