/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#if !defined(CORE_H_SEPTEMBER_14_2012)
#define CORE_H_SEPTEMBER_14_2012

#include <cycfi/config.hpp>
#include <cycfi/binary.hpp>
#include <stdint.h>

namespace cycfi
{
   void delay_ms(int32_t ms);

#ifndef assert
#define assert(x)
#endif

#if (CYCFI_LIB_MCU == MSP430)

   inline void delay_ms(int32_t ms)
   {
      int32_t const ms_period = 1000;  // 1ms = 1 / 1000s
      while (--ms)
         _delay_cycles(clock_speed / ms_period);
   }

   namespace detail
   {
      template <int ID>
      struct port_access;

      template <>
      struct port_access<1>
      {
         static volatile uint8_t& dir() { return P1DIR; }
         static volatile uint8_t& get() { return P1OUT; }
      };

      template <>
      struct port_access<2>
      {
         static volatile uint8_t& dir() { return P2DIR; }
         static volatile uint8_t& get() { return P2OUT; }
      };
   }

   inline void enable_timer_interrupt1()
   {
      CCTL0 = CCIE;
   }

   inline void disable_timer_interrupt1()
   {
      CCTL0 &= ~CCIE;
   }

   inline void enable_timer_interrupt2()
   {
      CCTL1 = CCIE;
   }

   inline void disable_timer_interrupt2()
   {
      CCTL1 &= ~CCIE;
   }

   inline void enable_WDT_interrupt()
   {
	   IE1 |= WDTIE;
   }

   inline void disable_WDT_interrupt()
   {
	   IE1 &= ~WDTIE;
   }


#endif

   template <int ID>
   struct port
   {
      port(int8_t io) { detail::port_access<ID>::dir() = io; }
      operator volatile uint8_t&() { return detail::port_access<ID>::get(); }
   };

   template <int ID, unsigned bit>
   struct port_bit
   {
      port_bit(bool write = false)
      {
         if (write)
            detail::port_access<ID>::dir() |= (1 << bit);
         else
            detail::port_access<ID>::dir() &= ~(1 << bit);
      }

      operator bool() { return (detail::port_access<ID>::get() & (1 << bit)) != 0; }

      port_bit& operator=(bool val)
      {
         if (val)
            detail::port_access<ID>::get() |= (1 << bit);
         else
            detail::port_access<ID>::get() &= ~(1 << bit);
         return *this;
      }
   };

}

#endif


