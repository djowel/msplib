/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#include <msplib.hpp>
#include<msplib/analog.hpp>

namespace msplib
{
	void main()
	{
		analog_pin adc;
		output_pin<0> led;
		led = 0;

		while (1)
		{
			adc.start(1);
			uint16_t val = adc.read();
			int threshold = 250; // With Hysteresis

			if (val > threshold)
			{
				led = 1;
				threshold = 250;
			}
			else
			{
				led = 0;
				threshold = 750;
			}
		}
	}
}

#include <msplib_main_impl.hpp>
