/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#if !defined(MSP430_TIMING_HPP_OCTOBER_8_2012)
#define MSP430_TIMING_HPP_OCTOBER_8_2012

#include <msp430.h>
#include <stdint.h>

namespace msplib
{
   inline void delay_cycles(uint32_t delay)
   {
      _delay_cycles(delay);
   }

   inline void noop()
   {
      _NOP();
   }

   inline void sleep()
   {
       _bis_SR_register(LPM0_bits + GIE);
   }

   extern uint32_t tar_counter;

   inline uint32_t sys_tick()
   {
      return TAR | (uint32_t(tar_counter) << 16);
   }

   // TODO: move this to main.ipp using WDT
   inline void delay_ms(uint32_t ms)
   {
      uint32_t const khz = 1000;       // 1 KHZ
      while (--ms)
         _delay_cycles(clock_speed / khz);
   }

   inline void delay_us(uint32_t us)
   {
      uint32_t const mhz = 1000000;    // 1 MHZ
      while (--us)
         _delay_cycles(clock_speed / mhz);
   }
}

#endif
