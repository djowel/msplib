/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#if !defined(MSP430_PORT_HPP_OCTOBER_8_2012)
#define MSP430_PORT_HPP_OCTOBER_8_2012

#include <msp430.h>
#include <stdint.h>
#include <msplib/memreg.hpp>

namespace msplib
{
   ////////////////////////////////////////////////////////////////////////////
   // output_port(s)
   ////////////////////////////////////////////////////////////////////////////
   template <>
   struct output_port<0> : m_register8<P1OUT>
   {
      output_port() { P1DIR = 0xFF; }
   };

   template <>
   struct output_port<1> : m_register8<P2OUT>
   {
      output_port() { P2DIR = 0xFF; }
   };

   ////////////////////////////////////////////////////////////////////////////
   // input_port(s)
   ////////////////////////////////////////////////////////////////////////////
   template <>
   struct input_port<0> : m_register8<P1IN>
   {
      input_port() { P1DIR = 0; }
   };

   template <>
   struct input_port<1> : m_register8<P2IN>
   {
      input_port() { P2DIR = 0; }
   };

   // $$$ TODO add more but guard using ifdef based on number of ports for
   // $$$ specific chips
}

#endif
