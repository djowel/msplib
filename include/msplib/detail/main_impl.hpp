/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#include <msplib.hpp>

namespace msplib
{
   msplib::task<1>* task1;
   msplib::task<2>* task2;
   msplib::task<3>* task3;
   msplib::task<4>* task4;
   msplib::task<5>* task5;
   msplib::task<6>* task6;

   // Most significant 16 bits of the 32 bit timer
   // This will be incremented when TAR overflows
   uint32_t tar_counter;
}

int main()
{
   WDTCTL = WDTPW + WDTHOLD;                 // Stop WDT
   
   switch (msplib::clock_speed)
   {
      case 16000000:
         BCSCTL1 = CALBC1_16MHZ;
         DCOCTL = CALDCO_16MHZ;
         break;
      
      case 8000000:
         BCSCTL1 = CALBC1_8MHZ;
         DCOCTL = CALDCO_8MHZ;
         break;
      
      case 12000000:
         BCSCTL1 = CALBC1_12MHZ;
         DCOCTL = CALDCO_12MHZ;
         break;
      
      case 1000000:
      default:
         BCSCTL1 = CALBC1_1MHZ;
         DCOCTL = CALDCO_1MHZ;
         break;   
   }
   
   #if !defined(ENABLE_TOUCH_SENSE)
  
      TA0CCTL0 = CCIE;                          // TA0CCR0 interrupt enabled
      TA0CCTL1 = CCIE;                          // TA0CCR1 interrupt enabled
      TA0CCTL2 = CCIE;                          // TA0CCR2 interrupt enabled
      TA1CCTL0 = CCIE;                          // TA1CCR0 interrupt enabled
      TA1CCTL1 = CCIE;                          // TA1CCR1 interrupt enabled
      TA1CCTL2 = CCIE;                          // TA1CCR2 interrupt enabled
      
      TA0CCR0 = 0;
      TA0CCR1 = 0;
      TA0CCR2 = 0;

      TA1CCR0 = 0;
      TA1CCR1 = 0;
      TA1CCR2 = 0;

      TA0CTL = TASSEL_2 + MC_2 + TAIE;
      TA1CTL = TA0CTL;  
   
   #else
   
      TA1CCTL0 = CCIE;                          // TA1CCR0 interrupt enabled
      TA1CCTL1 = CCIE;                          // TA1CCR1 interrupt enabled
      TA1CCTL2 = CCIE;                          // TA1CCR2 interrupt enabled
      
      TA1CCR0 = 0;
      TA1CCR1 = 0;
      TA1CCR2 = 0;
      
      TA1CTL = TASSEL_2 + MC_2 + TAIE;
   
      BCSCTL3 |= LFXT1S_2;                      // LFXT1 = VLO
      IE1 |= WDTIE;                             // enable WDT interrupt
      P2SEL = 0x00;                             // No XTAL
        
   #endif
   
   P1DIR = 0xFF;
   P1OUT = 0;
   P1IFG = 0;
   
   #define MSPLIB_PORT_INITIALIZATION(OUT, DIR)                                      \
      DIR = 0xFF;                                                                    \
      OUT = 0;                                                                       \
   /***/

   #define MSPLIB_PORT_INTERRUPT_INITIALIZATION(FLAG)                                \
      FLAG = 0;                                                                      \
   /***/

   //Port initialization
   #if defined(__MSP430_HAS_PORT2_R__)
   MSPLIB_PORT_INITIALIZATION(P2OUT, P2DIR)
   #endif
   
   #if defined(__MSP430_HAS_PORT3_R__)
   MSPLIB_PORT_INITIALIZATION(P3OUT, P3DIR)
   #endif
   
   #if defined(__MSP430_HAS_PORT4_R__)
   MSPLIB_PORT_INITIALIZATION(P4OUT, P4DIR)
   #endif
   
   #if defined(__MSP430_HAS_PORT5_R__)
   MSPLIB_PORT_INITIALIZATION(P5OUT, P5DIR)
   #endif
     
   #if defined(__MSP430_HAS_PORT6_R__)
   MSPLIB_PORT_INITIALIZATION(P6OUT, P6DIR)
   #endif  
   
   #if defined(__MSP430_HAS_PORT7_R__)
   MSPLIB_PORT_INITIALIZATION(P7OUT, P7DIR)
   #endif  
   
   #if defined(__MSP430_HAS_PORT8_R__)
   MSPLIB_PORT_INITIALIZATION(P8OUT, P8DIR)
   #endif  
   
   //Port interrupt initialization
   
   #if defined(PORT2_VECTOR)
   MSPLIB_PORT_INTERRUPT_INITIALIZATION(P2IFG)
   #endif
   
   #if defined(PORT3_VECTOR)
   MSPLIB_PORT_INTERRUPT_INITIALIZATION(P3IFG)
   #endif
   
   #if defined(PORT4_VECTOR)
   MSPLIB_PORT_INTERRUPT_INITIALIZATION(P4IFG)
   #endif
   
   #if defined(PORT5_VECTOR)
   MSPLIB_PORT_INTERRUPT_INITIALIZATION(P5IFG)
   #endif
     
   #if defined(PORT6_VECTOR)
   MSPLIB_PORT_INTERRUPT_INITIALIZATION(P6IFG)
   #endif  
   
   #if defined(PORT7_VECTOR)
   MSPLIB_PORT_INTERRUPT_INITIALIZATION(P7IFG)
   #endif  
   
   #if defined(PORT8_VECTOR)
   MSPLIB_PORT_INTERRUPT_INITIALIZATION(P8IFG)
   #endif  
 
   _EINT();

   msplib::task<1> task1_;
   msplib::task1 = &task1_;
   msplib::task<2> task2_;
   msplib::task2 = &task2_;
   msplib::task<3> task3_;
   msplib::task3 = &task3_;
   msplib::task<4> task4_;
   msplib::task4 = &task4_;
   msplib::task<5> task5_;
   msplib::task5 = &task5_;
   msplib::task<6> task6_;
   msplib::task6 = &task6_;
   msplib::main();
}

#if !defined(ENABLE_TOUCH_SENSE)

#pragma vector=TIMER0_A0_VECTOR
__interrupt void timer0_A0_ISR()
{
   TA0CCR0 += msplib::task1->period;
   msplib::task1->perform();
}

#pragma vector=TIMER0_A1_VECTOR
__interrupt void timer0_A1_ISR()
{
   switch (TA0IV)
   {
      case 2:
         TA0CCR1 += msplib::task2->period;
         msplib::task2->perform();
         break;

      case 4:
         TA0CCR2 += msplib::task3->period;
         msplib::task3->perform();
         break;

      case 10:                               // overflow
         TA0CTL &= ~TAIFG;
         msplib::tar_counter++;
         break;
   }
}

#else

#pragma vector=WDT_VECTOR
__interrupt void wdt_ISR()
{
  TA0CCTL1 ^= CCIS0;                        // Create SW capture of CCR1
  __bic_SR_register_on_exit(LPM0_bits);     // Exit LPM3 on reti
}

#endif


#pragma vector = TIMER1_A0_VECTOR
__interrupt void timer1_A0_ISR()
{
   TA1CCR0 += msplib::task4->period;
   msplib::task4->perform();
}

#pragma vector = TIMER1_A1_VECTOR
__interrupt void timer1_A1_ISR()
{
   switch(TA1IV)
   {
      case 2:
         TA1CCR1 += msplib::task5->period;
         msplib::task5->perform();
         break;

      case 4:
         TA1CCR2 += msplib::task6->period;
         msplib::task6->perform();
         break;

      case 10:                                  // overflow
         TA1CTL &= ~TAIFG;
         break;
   }
}

#pragma vector = PORT1_VECTOR
__interrupt void port1_ISR()
{
   uint32_t time = msplib::sys_tick();
   uint16_t bit_mask = P1IFG & 0xFF;
   P1IFG = 0;                                   // clear port1 interrupt flags
//   if (msplib::pin_interrupt<1>::call(bit_mask, time))
//      _bic_SR_register_on_exit(LPM0_bits);    // exit low power mode
}

#define MSPLIB_PORT_ISR(FLAG)                                                  \
   uint32_t time = msplib::sys_tick();                                         \
   uint16_t bit_mask = FLAG & 0xFF;                                           \
   FLAG = 0;                                   // clear port1 interrupt flags \
//   if (msplib::pin_interrupt<1>::call(bit_mask, time))                       \
//      _bic_SR_register_on_exit(LPM0_bits);    // exit low power mode        \
/*****/

#if defined(PORT2_VECTOR)
#pragma vector = PORT2_VECTOR
__interrupt void port2_ISR()
{
   MSPLIB_PORT_ISR(P2IFG)
}
#endif

#if defined(PORT3_VECTOR)
#pragma vector = PORT3_VECTOR
__interrupt void port3_ISR()
{
   MSPLIB_PORT_ISR(P3IFG)
}
#endif

#if defined(PORT4_VECTOR)
#pragma vector = PORT4_VECTOR
__interrupt void port4_ISR()
{
   MSPLIB_PORT_ISR(P4IFG)
}
#endif

#if defined(PORT5_VECTOR)
#pragma vector = PORT5_VECTOR
__interrupt void port5_ISR()
{
   MSPLIB_PORT_ISR(P5IFG)
}
#endif

#if defined(PORT6_VECTOR)
#pragma vector = PORT6_VECTOR
__interrupt void port6_ISR()
{
   MSPLIB_PORT_ISR(P6IFG)
}
#endif

#if defined(PORT7_VECTOR)
#pragma vector = PORT7_VECTOR
__interrupt void port7_ISR()
{
   MSPLIB_PORT_ISR(P7IFG)
}
#endif

#if defined(PORT8_VECTOR)
#pragma vector = PORT8_VECTOR
__interrupt void port8_ISR()
{
   MSPLIB_PORT_ISR(P8IFG)
}
#endif















