/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#if !defined(MPS430_IMPL_LITTLE_ENDIAN_HPP_OCTOBER_8_2012)
#define MPS430_IMPL_LITTLE_ENDIAN_HPP_OCTOBER_8_2012

#include <stdint.h>

namespace msplib
{
   // The MSP430 MCU is little-endian, so no need to do anything here

   inline void little_endian(int16_t& val) {}
   inline void little_endian(uint16_t& val) {}
   inline void little_endian(int32_t& val) {}
   inline void little_endian(uint32_t& val) {}
   inline void little_endian(int64_t& val) {}
   inline void little_endian(uint64_t& val) {}
   inline void little_endian(float& val) {}
   inline void little_endian(double& val) {}
}

#endif
