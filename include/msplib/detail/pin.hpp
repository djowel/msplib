/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#if !defined(MSP430_PIN_HPP_OCTOBER_19_2012)
#define MSP430_PIN_HPP_OCTOBER_19_2012

#include <msp430.h>
#include <stdint.h>
#include <msplib/pin.hpp>
#include <msplib/constants.hpp>

#ifdef N
#undef N
#endif

namespace msplib
{
   ////////////////////////////////////////////////////////////////////////////
   // output_pin(s)
   ////////////////////////////////////////////////////////////////////////////
   template <int bit, int port = bit / 8>
   struct output_pin_port_impl;

   template <int bit>
   struct output_pin_port_impl<bit, 0> : pin_base<uint8_t, P1OUT, bit>
   {
      output_pin_port_impl() { P1DIR |= 1 << bit; }
      using pin_base<uint8_t, P1OUT, bit>::operator=;
   };
   
   #define MSPLIB_DEF_OUTPUT_PIN_PORT_IMPL(N, OUT, DIR)                       \
      template <int bit>                                                      \
      struct output_pin_port_impl<bit, N> : pin_base<uint8_t, OUT, bit % 8>   \
      {                                                                       \
         static int const bit_ = bit % 8;                                     \
         output_pin_port_impl() { DIR |= 1 << bit_; }                         \
         using pin_base<uint8_t, OUT, bit_>::operator=;                       \
      };                                                                      \
   /***/

   #if defined(__MSP430_HAS_PORT2_R__)
   MSPLIB_DEF_OUTPUT_PIN_PORT_IMPL(1, P2OUT, P2DIR)
   #endif
   
   #if defined(__MSP430_HAS_PORT3_R__)
   MSPLIB_DEF_OUTPUT_PIN_PORT_IMPL(2, P3OUT, P3DIR)
   #endif
   
   #if defined(__MSP430_HAS_PORT4_R__)
   MSPLIB_DEF_OUTPUT_PIN_PORT_IMPL(3, P4OUT, P4DIR)
   #endif
   
   #if defined(__MSP430_HAS_PORT5_R__)
   MSPLIB_DEF_OUTPUT_PIN_PORT_IMPL(4, P5OUT, P5DIR)
   #endif
     
   #if defined(__MSP430_HAS_PORT6_R__)
   MSPLIB_DEF_OUTPUT_PIN_PORT_IMPL(5, P6OUT, P6DIR)
   #endif  
   
   #if defined(__MSP430_HAS_PORT7_R__)
   MSPLIB_DEF_OUTPUT_PIN_PORT_IMPL(6, P7OUT, P7DIR)
   #endif  
   
   #if defined(__MSP430_HAS_PORT8_R__)
   MSPLIB_DEF_OUTPUT_PIN_PORT_IMPL(7, P8OUT, P8DIR)
   #endif  
   
   template <int bit>
   struct output_pin : output_pin_port_impl<bit>
   {
      using output_pin_port_impl<bit>::operator=;
   };

   ////////////////////////////////////////////////////////////////////////////
   // input_pin(s)
   ////////////////////////////////////////////////////////////////////////////
   template <int bit, int port = bit / 8>
   struct input_pin_port_impl;

   template <int bit>
   struct input_pin_port_impl<bit, 0> : pin_base<uint8_t, P1IN, bit>
   {
      input_pin_port_impl() { P1DIR &= ~(1 << bit); }
      using pin_base<uint8_t, P1IN, bit>::operator=;
   };
   
   #define MSPLIB_DEF_INPUT_PIN_PORT_IMPL(N, IN, DIR)                          \
      template <int bit>                                                       \
      struct input_pin_port_impl<bit, N> : pin_base<uint8_t, IN, bit % 8>      \
      {                                                                        \
         static int const bit_ = bit % 8;                                      \
         input_pin_port_impl() { DIR &= ~(1 << bit_); }                        \
         using pin_base<uint8_t, IN, bit_>::operator=;                         \
      };                                                                       \
      /***/
      
   #if defined(__MSP430_HAS_PORT2_R__)
   MSPLIB_DEF_INPUT_PIN_PORT_IMPL(1, P2IN, P2DIR)
   #endif
   
   #if defined(__MSP430_HAS_PORT3_R__)
   MSPLIB_DEF_INPUT_PIN_PORT_IMPL(2, P3IN, P3DIR)
   #endif
   
   #if defined(__MSP430_HAS_PORT4_R__)
   MSPLIB_DEF_INPUT_PIN_PORT_IMPL(3, P4IN, P4DIR)
   #endif
   
   #if defined(__MSP430_HAS_PORT5_R__)
   MSPLIB_DEF_INPUT_PIN_PORT_IMPL(4, P5IN, P5DIR)
   #endif
     
   #if defined(__MSP430_HAS_PORT6_R__)
   MSPLIB_DEF_INPUT_PIN_PORT_IMPL(5, P6IN, P6DIR)
   #endif  
   
   #if defined(__MSP430_HAS_PORT7_R__)
   MSPLIB_DEF_INPUT_PIN_PORT_IMPL(6, P7IN, P7DIR)
   #endif  
   
   #if defined(__MSP430_HAS_PORT8_R__)
   MSPLIB_DEF_INPUT_PIN_PORT_IMPL(7, P8IN, P8DIR)
   #endif  
      
   template <int bit>
   struct input_pin : input_pin_port_impl<bit>
   {
      using input_pin_port_impl<bit>::operator=;
   };

   ////////////////////////////////////////////////////////////////////////////
   // peripheral_select
   ////////////////////////////////////////////////////////////////////////////  
   namespace detail
   {
      void peripheral_select(int port, int bit_mask, mode::peripheral_enum select);
   }
   
   template <int N>
   void peripheral_select(mode::peripheral_enum select)
   {
      int const port = N / 8;
      int const bit = N % 8;
      int const bit_mask = 1 << bit;
      detail::peripheral_select(port, bit_mask, select);
   }
}

#endif
