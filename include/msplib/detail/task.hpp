/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#if !defined(MSP430_TASK_HPP_OCTOBER_8_2012)
#define MSP430_TASK_HPP_OCTOBER_8_2012

#include <msp430.h>
#include <stdint.h>
#include <msplib/memreg.hpp>

namespace msplib
{
  namespace detail
  {
      template <int ID>
      struct timer;

      template <>
      struct timer<1>
      {
         typedef m_register16<TA0CCR0> counter_type;

         static void enable()
         {
            TA0CCTL0 = CCIE;
         }

         static void disable()
         {
            TA0CCTL0 &= ~CCIE;
         }
      };

      template <>
      struct timer<2>
      {
         typedef m_register16<TA0CCR1> counter_type;

         static void enable()
         {
            TA0CCTL1 = CCIE;
         }

         static void disable()
         {
            TA0CCTL1 &= ~CCIE;
         }
      };

      template <>
      struct timer<3>
      {
         typedef m_register16<TA0CCR2> counter_type;

         static void enable()
         {
            TA0CCTL2 = CCIE;
         }

         static void disable()
         {
            TA0CCTL2 &= ~CCIE;
         }
      };

      template <>
      struct timer<4>
      {
         typedef m_register16<TA1CCR0> counter_type;

         static void enable()
         {
            TA1CCTL0 = CCIE;
         }

         static void disable()
         {
            TA1CCTL0 &= ~CCIE;
         }
      };

      template <>
      struct timer<5>
      {
         typedef m_register16<TA1CCR1> counter_type;

         static void enable()
         {
            TA1CCTL1 = CCIE;
         }

         static void disable()
         {
            TA1CCTL1 &= ~CCIE;
         }
      };

      template <>
      struct timer<6>
      {
         typedef m_register16<TA1CCR2> counter_type;

         static void enable()
         {
            TA1CCTL2 = CCIE;
         }

         static void disable()
         {
            TA1CCTL2 &= ~CCIE;
         }
      };
  }
}

#endif
