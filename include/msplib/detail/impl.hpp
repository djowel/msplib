/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#if !defined(MSP430_IMPL_HPP_SEPTEMBER_18_2012)
#define MSP430_IMPL_HPP_SEPTEMBER_18_2012

#include <msp430.h>
#include <stdint.h>
#include <msplib/memreg.hpp>
#include <msplib/detail/timing.hpp>
#include <msplib/detail/port.hpp>
#include <msplib/detail/pin.hpp>
#include <msplib/detail/task.hpp>

#endif

