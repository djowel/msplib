/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#if !defined(TASK_HPP_OCTOBER_8_2012)
#define TASK_HPP_OCTOBER_8_2012

#include <stdint.h>

namespace msplib
{
   ////////////////////////////////////////////////////////////////////////////
   // Tasks
   ////////////////////////////////////////////////////////////////////////////
   namespace detail
   {
      template <int ID>
      struct timer;
   }

   template <int ID>
   struct task_base
   {
      static void enable()
      {
         detail::timer<ID>::enable();
      }

      static void disable()
      {
         detail::timer<ID>::disable();
      }

      typename detail::timer<ID>::counter_type counter;
   };

   template <int ID>
   struct task : task_base<ID>
   {
      static uint16_t const period = 0;

      task()
      {
         this->disable();
      }

      void perform() {}
   };
}

#endif
