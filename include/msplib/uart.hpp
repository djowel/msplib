/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#if !defined(UART_HPP_OCTOBER_8_2012)
#define UART_HPP_OCTOBER_8_2012

#include <stdint.h>
#include <msplib.hpp>

namespace msplib
{
   class uart
   {
   public:

      uart(uint32_t baud);

      bool write_ready() const
      {
         // USCI_A0 TX buffer ready?
         return IFG2&UCA0TXIFG;
      }
      
      void write(uint8_t data);
      void write(uint8_t const* ptr, uint32_t len)
      {
         while (len--)
            write(*ptr++);
      }

      bool read_ready() const
      {
         // USCI_A0 RX buffer ready?
         return IFG2&UCA0RXIFG;
      }

      void read(uint8_t& data);
      void read(uint8_t* ptr, uint32_t len)
      {
         while (len--)
            read(*ptr++);
      }
   };
}

#endif
