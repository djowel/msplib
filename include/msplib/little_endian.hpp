/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#if !defined(LITTLE_ENDIAN_HPP_OCTOBER_8_2012)
#define LITTLE_ENDIAN_HPP_OCTOBER_8_2012

#include <stdint.h>

namespace msplib
{
   void little_endian(int16_t& val);
   void little_endian(uint16_t& val);
   void little_endian(int32_t& val);
   void little_endian(uint32_t& val);
   void little_endian(int64_t& val);
   void little_endian(uint64_t& val);
   void little_endian(float& val);
   void little_endian(double& val);
}

#if (MSPLIB_LIB_MCU == MSP430)
# include <msplib/detail/little_endian.hpp>
#endif

#endif
