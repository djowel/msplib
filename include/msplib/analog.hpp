/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#if !defined(ANALOG_HPP_OCTOBER_23_2012)
#define ANALOG_HPP_OCTOBER_23_2012
#include <msplib.hpp>

namespace msplib
{   
   struct analog_pin
   {
   public:

      analog_pin();
      void start(int channel) const;

      bool ready() const
      {
         // Is the ADC ready yet?
         return !(ADC10CTL1 & ADC10BUSY);
      }

      uint16_t read() const;
   };
}
#endif

