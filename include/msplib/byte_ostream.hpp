/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#if !defined(BYTE_OSTREAM_HPP_OCTOBER_8_2012)
#define BYTE_OSTREAM_HPP_OCTOBER_8_2012

#include <stdint.h>
#include <cstring>
#include <msplib/little_endian.hpp>
#include <msplib/opaque.hpp>

namespace msplib
{
   ////////////////////////////////////////////////////////////////////////////
   // Byte oriented output stream. Only fundamental types, C strings, and
   // opaque objects are supported. Fundamental types more than 8 bits
   // (e.g. int32_t) are streamed out as little endian.
   ////////////////////////////////////////////////////////////////////////////
   template <typename Port>
   class byte_ostream
   {
   public:

      byte_ostream(Port& port)
         : port(port) {}

      byte_ostream& operator<<(bool val)
      {
         port.write(uint8_t(val));
         return *this;
      }

      byte_ostream& operator<<(char val)
      {
         port.write(uint8_t(val));
         return *this;
      }

      byte_ostream& operator<<(uint8_t val)
      {
         port.write(val);
         return *this;
      }

      // template <typename T>
      // byte_ostream& operator<<(T val)
      // {
         // little_endian(val);
         // port.write(reinterpret_cast<uint8_t*>(&val), sizeof(T));
         // return *this;
      // }
      
      byte_ostream& operator<<(int16_t val)
      {
         little_endian(val);
         port.write(reinterpret_cast<uint8_t*>(&val), sizeof(int16_t));
         return *this;
      }
      
      byte_ostream& operator<<(uint16_t val)
      {
         little_endian(val);
         port.write(reinterpret_cast<uint8_t*>(&val), sizeof(uint16_t));
         return *this;
      }
      
      byte_ostream& operator<<(int32_t val)
      {
         little_endian(val);
         port.write(reinterpret_cast<uint8_t*>(&val), sizeof(int32_t));
         return *this;
      }
      
      byte_ostream& operator<<(uint32_t val)
      {
         little_endian(val);
         port.write(reinterpret_cast<uint8_t*>(&val), sizeof(uint32_t));
         return *this;
      }
      
      byte_ostream& operator<<(int64_t val)
      {
         little_endian(val);
         port.write(reinterpret_cast<uint8_t*>(&val), sizeof(int64_t));
         return *this;
      }
      
      byte_ostream& operator<<(uint64_t val)
      {
         little_endian(val);
         port.write(reinterpret_cast<uint8_t*>(&val), sizeof(uint64_t));
         return *this;
      }
      
      byte_ostream& operator<<(float val)
      {
         little_endian(val);
         port.write(reinterpret_cast<uint8_t*>(&val), sizeof(float));
         return *this;
      }
      
      byte_ostream& operator<<(double val)
      {
         little_endian(val);
         port.write(reinterpret_cast<uint8_t*>(&val), sizeof(double));
         return *this;
      }

      byte_ostream& operator<<(char const* s)
      {
         port.write(reinterpret_cast<uint8_t const*>(s), std::strlen(s));
         return *this;
      }

      byte_ostream& operator<<(opaque const& val)
      {
         port.write(val.data, val.size);
         return *this;
      }

   private:

      Port& port;
   };
}

#endif
