/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#if !defined(OPAQUE_HPP_OCTOBER_8_2012)
#define OPAQUE_HPP_OCTOBER_8_2012

#include <stdint.h>

namespace msplib
{
   ////////////////////////////////////////////////////////////////////////////
   // Low level string, opaque class. Some types have variable sizes. Reading
   // these types of data requires a pointer to a buffer of adequate size.
   // This class encapsulates such information.
   ////////////////////////////////////////////////////////////////////////////
   struct opaque
   {
      opaque()
         : data(0), size(0) {}

      opaque(uint8_t* data, uint32_t size)
         : data(data), size(size) {}

      uint8_t* data;
      uint32_t size;
   };
}

#endif
