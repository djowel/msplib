/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#if !defined(MSPLIB_HPP_SEPTEMBER_18_2012)
#define MSPLIB_HPP_SEPTEMBER_18_2012

#include <stdint.h>
#include <cstdlib>

#include <msplib/memreg.hpp>
#include <msplib/timing.hpp>
#include <msplib/port.hpp>
#include <msplib/pin.hpp>
#include <msplib/task.hpp>

// Define for the MCU used
#define MSPLIB_LIB_MCU MSP430

#if (MSPLIB_LIB_MCU == MSP430)
# include <msplib/detail/impl.hpp>
#endif

#endif
