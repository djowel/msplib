/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#include <msplib/detail/pin.hpp>
#include <msplib/constants.hpp>
#include <msplib.hpp>

namespace msplib
{
   namespace detail
   {
      void peripheral_select(int port, int bit_mask, mode::peripheral_enum select)
      {
         switch (port)
         {
         
            #define MSPLIB_FUNCTION_SELECT(n, FUNC1_SEL, FUNC2_SEL)             \
            case n:                                                            \
               switch (select)                                                 \
               {                                                               \
                  case mode::io_port:                                          \
                     FUNC1_SEL &= ~bit_mask;                                   \
                     FUNC2_SEL &= ~bit_mask;                                   \
                     break;                                                    \
                                                                               \
                  case mode::pin_osc:                                          \
                     FUNC1_SEL &= ~bit_mask;                                   \
                     FUNC2_SEL |= bit_mask;                                    \
                     break;                                                    \
               }                                                               \
               break;                                                          \
              /***/
            
            #if defined(__MSP430_HAS_PORT1_R__)
            MSPLIB_FUNCTION_SELECT(0, P1SEL, P1SEL2)
            #endif
            
            #if defined (__MSP430_HAS_PORT2_R__)
            MSPLIB_FUNCTION_SELECT(1, P2SEL, P2SEL2)
            #endif
            
            #if defined (__MSP430_HAS_PORT3_R__)
            MSPLIB_FUNCTION_SELECT(2, P3SEL, P3SEL2)
            #endif    

            #if defined (__MSP430_HAS_PORT4_R__)
            MSPLIB_FUNCTION_SELECT(3, P4SEL, P4SEL2)
            #endif    

            #if defined (__MSP430_HAS_PORT5_R__)
            MSPLIB_FUNCTION_SELECT(4, P5SEL, P5SEL2)
            #endif                
             
            #if defined (__MSP430_HAS_PORT6_R__)
            MSPLIB_FUNCTION_SELECT(5, P6SEL, P6SEL2)
            #endif   

            #if defined (__MSP430_HAS_PORT7_R__)
            MSPLIB_FUNCTION_SELECT(6, P7SEL, P7SEL2)
            #endif  

            #if defined (__MSP430_HAS_PORT8_R__)
            MSPLIB_FUNCTION_SELECT(7, P8SEL, P8SEL2)
            #endif                
         }
      }
   }
}



