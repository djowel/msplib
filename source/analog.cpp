/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#include <msplib.hpp>
#include <stdint.h>
#include <msplib/analog.hpp>

namespace msplib
{
   analog_pin::analog_pin()
   {
      // ADC10ON, 4 x ADC10CLKs
      ADC10CTL0 = ADC10SHT_0 + ADC10ON;
   }

   void analog_pin::start(int channel) const
   {
      // Set ADC channel
      ADC10CTL1 = (channel * 0x1000u);
      ADC10AE0 = 1 << channel;
      
      // Sampling and conversion start
      ADC10CTL0 |= ADC10SC + ENC; 
   }

   uint16_t analog_pin::read() const
   {
      while (!ready())
         ;
      return ADC10MEM;
   }
}

