/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#include <msplib/uart.hpp>
#include <msplib.hpp>
#include <math.h>

namespace msplib
{
   uart::uart(uint32_t baud)
   {
      uint8_t mask;
      uint16_t bits = msplib::clock_speed/baud;
      P1SEL  = BIT1 + BIT2;
      P1SEL2 = BIT1 + BIT2;
      UCA0CTL1 |= UCSSEL_2;
      UCA0BR0 = bits & 0xFF;
      UCA0BR1 = bits >> 8;

      if (bits >= 16)
         mask = round(((bits/16)-int(bits/16))*16);
      else
         mask = round(bits- int(bits)*8);

      UCA0MCTL =  mask;
      UCA0CTL1 &= ~UCSWRST;                   // Initialize USCI state machine

      __bis_SR_register(GIE);
   }

   void uart::write(uint8_t data)
   {
      IE2 |= UCA0TXIE;                        // enable TX interrupt
      while (!write_ready())
         ;
      UCA0TXBUF =  data;
   }

   void uart::read(uint8_t& data)
   {
      IE2 |= UCA0RXIE;                        // enable RX interrupt
      while (!read_ready())
         ;                  
      data = UCA0RXBUF;
   }
}

#pragma vector = USCIAB0TX_VECTOR;
__interrupt void USCIA0TX_ISR()
{
   IE2 &= ~UCA0TXIE;                          // disable interrupt of TX
}

#pragma vector = USCIAB0RX_VECTOR;
__interrupt void USCIA0RX_ISR (void)
{
   IE2 &= ~UCA0RXIE;                          // disable interrupt of RX
}

