/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#include <msplib.hpp>
#include <msplib/uart.hpp>

namespace msplib
{
   void main()
   {
      uart uart(9600);

      while (1)
      {
    	 uint8_t ch = 0;
    	 uart.read(ch);
         uart.write(ch);
      }
   }
}

#include <msplib_main_impl.hpp>
