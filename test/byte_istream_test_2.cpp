/*=============================================================================
Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#include <cycfi.hpp>
#include <cycfi/uart.hpp>
#include <cycfi/byte_ostream.hpp>
#include <cycfi/byte_istream.hpp>

namespace cycfi
{
   void main()
   {
      uart port(9600);
      byte_ostream<uart> out(port);
      byte_istream<uart> in(port);

      while (1)
      {
         uint8_t a, b, c, d;
         in >> a >> b >> c >> d;
         out << a << b << c << d;

         delay_ms(1000);
      }
   }
}

#include <cycfi_main_impl.hpp>


