/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#include <msplib.hpp>


namespace msplib
{
   template <>
   struct task<1> : task_base<1>
   {
      static uint16_t const period = 1000;

      task() : i(0)
      {
         led = 1;
      }

      void perform()
      {
         if (i++ == 1000)
         {
            led = !led;
            i = 0;
         }
      }

      output_pin<0> led;  // port 1.0
      int i;
   };
   
   template <>
   struct task<2> : task_base<2>
   {
      static uint16_t const period = 1000;

      task() : i(0)
      {
         led = 1;
      }

      void perform()
      {
         if (i++ == 2000)
         {
            led = !led;
            i = 0;
         }
      }

      output_pin<6> led;  // port 1.6
      int i;
   };

   void main()
   {
     sleep();
   }
}

#include <msplib_main_impl.hpp>
