/*=============================================================================
  Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#include <msplib.hpp>
#include <msplib/uart.hpp>
#include <msplib/byte_ostream.hpp>

namespace msplib
{
   void main()
   {
      uart port(9600);
      byte_ostream<uart> out(port);

      while (1)
      {
         out << 'J' << 'e' << 'w' << 'a' << 'r' << 'd' << '\n';
         out << "Jeward\n";
         out << (uint16_t) 0xABCD;
         out << "\n";
         out << (uint32_t) 0xDEADBEEF;
         out << "\n";
         out << (uint64_t) 0xA0B0C0D0AFBFCFDF;
         out << "\n";
         out << (float) 1.0;
         out << "\n";
         out << (double) 1.0;
         out << "\n";

         out << '\n';
         delay_ms(100);
      }
   }
}

#include <msplib_main_impl.hpp>


