/*=============================================================================
Copyright (c) Cycfi Research, Inc.
=============================================================================*/
#include <msplib.hpp>
#include <msplib/uart.hpp>
#include <msplib/byte_ostream.hpp>
#include <msplib/byte_istream.hpp>

namespace msplib
{
   void main()
   {
      uart port(9600);
      byte_ostream<uart> out(port);
      byte_istream<uart> in(port);
      char a;

      while (1)
      {
         in >> a;
         out << a;
         delay_ms(100);
      }
   }
}

#include <msplib_main_impl.hpp>


